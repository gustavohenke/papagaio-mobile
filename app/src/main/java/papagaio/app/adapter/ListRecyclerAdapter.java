package papagaio.app.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class ListRecyclerAdapter< E, VH extends RecyclerView.ViewHolder > extends RecyclerView.Adapter< VH > {

    private final List< E > list = Collections.synchronizedList( new ArrayList< E >() );

    public List< E > getAll () {
        return new ArrayList<>( list );
    }

    public void add ( E item ) {
        list.add( item );
    }

    public void addAll ( Collection< ? extends E > collection ) {
        list.addAll( collection );
    }

    public E get ( int index ) {
        return list.get( index );
    }

    public void clear () {
        list.clear();
    }

    public int size () {
        return list.size();
    }

    public boolean isEmpty () {
        return list.isEmpty();
    }

    @Override
    public int getItemCount () {
        return size();
    }
}

package papagaio.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.util.Log;
import android.util.Size;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import papagaio.app.R;
import papagaio.app.feature.profile.ProfileActivity;
import papagaio.app.model.Listener;
import papagaio.app.model.location.GeocodeRequest;
import papagaio.app.model.location.Location;
import papagaio.app.model.misc.Image;
import papagaio.app.model.post.Post;
import papagaio.app.util.Constants;
import papagaio.app.util.DateFormat;
import papagaio.app.util.IconToggler;
import papagaio.app.util.LaunchIntentClickListener;

public class PostListAdapter extends ListRecyclerAdapter< Post, PostListAdapter.ViewHolder > {

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textContent;
        public TextView textUsername;
        public TextView textDate;
        public TextView textLikeCount;
        public TextView textLocation;
        public ImageView imageAvatar;
        public ImageView imagePhoto;
        public View buttonToggleLike;

        public ViewHolder ( View itemView ) {
            super( itemView );

            textContent = ( TextView ) itemView.findViewById( R.id.post_text_content );
            textUsername = ( TextView ) itemView.findViewById( R.id.post_text_author );
            textDate = ( TextView ) itemView.findViewById( R.id.post_text_date );
            textLocation = ( TextView ) itemView.findViewById( R.id.post_text_location );

            imageAvatar = ( ImageView ) itemView.findViewById( R.id.post_image_avatar );
            imagePhoto = ( ImageView ) itemView.findViewById( R.id.post_image_photo );
            textLikeCount = ( TextView ) itemView.findViewById( R.id.post_text_like_count );
            buttonToggleLike = itemView.findViewById( R.id.post_button_toggle_like );
        }
    }

    // ---------------------------------------------------------------------------------------------

    private static final ImageLoader imageLoader = ImageLoader.getInstance();
    private static Pattern PATTERN_TAG = Pattern.compile( "[#]([A-Za-z0-9-_]+)\\b" );
    private static Pattern PATTERN_USERNAME = Pattern.compile( "[@]([A-Za-z0-9-_]+)\\b" );
    private LikeAction onLikeListener;
    private Context context;
    private final IconToggler iconToggler;

    public PostListAdapter ( Context context ) {
        this.context = context;
        this.iconToggler = new IconToggler( context );
    }

    @Override
    public PostListAdapter.ViewHolder onCreateViewHolder ( ViewGroup parent, int viewType ) {
        View view = LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.listitem_post, parent, false );

        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder ( PostListAdapter.ViewHolder holder, int position ) {
        Post post = get( position );

        holder.textContent.setText( post.getText() );
        holder.textDate.setText( DateFormat.format( post.getCreatedAt() ) );

        {
            Intent intent = ProfileActivity.newIntent( context, post.getAuthor().getId() );
            LaunchIntentClickListener launcher = new LaunchIntentClickListener( intent );

            holder.textUsername.setText( post.getAuthor().getStandardUsername() );
            holder.textUsername.setOnClickListener( launcher );
        }

        String avatarUrl = post.getAuthor().getPhoto().getUrl( new Size( 40, 40 ) );
        imageLoader.displayImage( avatarUrl, holder.imageAvatar );

        if ( post.getPhotos().isEmpty() ) {
            holder.imagePhoto.setVisibility( View.GONE );
        } else {
            Image photo = post.getPhotos().get( 0 );
            holder.imagePhoto.setVisibility( View.VISIBLE );
            imageLoader.displayImage( photo.getUrl(), holder.imagePhoto );
        }

        holder.textLikeCount.setText( post.getLikesCount() > 0 ? post.getLikesCount() + "" : "" );

        holder.buttonToggleLike.setOnClickListener( new ToggleLikeListener( post ) );
        toggleLikeStatus( post, holder.buttonToggleLike );

        loadLocation( holder, post );

        Linkify.addLinks( holder.textContent, Linkify.WEB_URLS );
        Linkify.addLinks(
                holder.textContent,
                PATTERN_TAG,
                Constants.URL_APP_HASHTAG,
                null,
                new HashTagFilter()
        );

        Linkify.addLinks(
                holder.textContent,
                PATTERN_USERNAME,
                Constants.URL_APP_USERNAME,
                null,
                new UsernameFilter()
        );
    }

    @Override
    public int getItemCount () {
        return size();
    }

    // ---------------------------------------------------------------------------------------------

    public LikeAction getOnLikeListener () {
        return onLikeListener;
    }

    public void setOnLikeListener ( LikeAction onLikeListener ) {
        this.onLikeListener = onLikeListener;
    }

    /**
     * Atualiza a view de curtidas de um post.
     *
     * O número de curtidas é atualizado e o estado "ativo"/"inativo" é aplicado sobre o botão.
     *
     * @param   post        O post que foi (des)curtido
     * @param   likeButton  O botão que enviou a (des)curtida
     */
    public void toggleLikeStatus ( Post post, View likeButton ) {
        final Resources res = context.getResources();
        final boolean liked = post.isLiked();
        final int count = post.getLikesCount();

        ImageView icon = ( ImageView ) likeButton.findViewById( R.id.post_image_like_icon );
        TextView number = ( TextView ) likeButton.findViewById( R.id.post_text_like_count );

        final int numberColorId = liked
                ? R.color.listitem_footer_text_active
                : R.color.listitem_footer_text;

        number.setText( "" + ( count > 0 ? count : "" ) );
        number.setTextColor( res.getColor( numberColorId ) );

        iconToggler.toggle( liked, icon );
    }

    private void loadLocation ( final ViewHolder holder, Post post ) {
        final Location location = post.getLocation();

        holder.textLocation.setVisibility( View.GONE );
        holder.textLocation.setText( null );

        if ( location == null ) {
            return;
        }

        new GeocodeRequest( location ).execute( new Listener< String >() {
            @Override
            public void success ( String locationName ) {
                holder.textLocation.setVisibility( View.VISIBLE );
                holder.textLocation.setText( context.getString(
                        R.string.postlist_in_location,
                        locationName
                ));
            }

            @Override
            public void failure ( Exception exception ) {

            }
        });
    }

    // ---------------------------------------------------------------------------------------------

    class ToggleLikeListener implements View.OnClickListener {

        private final Post post;

        public ToggleLikeListener ( Post post ) {
            this.post = post;
        }

        @Override
        public void onClick ( View v ) {
            if ( onLikeListener != null ) {
                onLikeListener.onLike( post, v );
            }

            toggleLikeStatus( post, v );
        }
    }

    // ---------------------------------------------------------------------------------------------

    public interface LikeAction {
        /**
         * Evento de curtida de um post
         *
         * @param post  O post que foi (des)curtido
         * @param view  A view que disparou a (des)curtida
         */
        void onLike ( Post post, View view );
    }

    // ---------------------------------------------------------------------------------------------

    class HashTagFilter implements Linkify.TransformFilter {

        @Override
        public String transformUrl ( Matcher match, String url ) {
            return match.group( 1 );
        }
    }

    // ---------------------------------------------------------------------------------------------

    class UsernameFilter implements Linkify.TransformFilter {

        @Override
        public String transformUrl ( Matcher match, String url ) {
            return match.group( 1 );
        }
    }
}

package papagaio.app.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import papagaio.app.model.misc.Image;

public class ImageAdapter extends ArrayAdapter< Image > {

    private static final ImageLoader imageLoader = ImageLoader.getInstance();

    public ImageAdapter ( Context context ) {
        super( context, 0 );
    }

    @Override
    public View getView ( int position, View convertView, ViewGroup parent ) {
        Image image = getItem( position );
        ImageView view;

        if ( convertView == null ) {
            view = new ImageView( getContext() );
            convertView = view;
        } else {
            view = ( ImageView ) convertView;
        }

        imageLoader.displayImage( image.getUrl(), view );
        return convertView;
    }
}

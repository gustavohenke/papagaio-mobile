package papagaio.app.service;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;

import papagaio.app.App;
import papagaio.app.authenticator.AccountAuthenticator;
import papagaio.app.rest.RestAPI;
import papagaio.app.util.Constants;

public class AuthService extends Service {

    private AccountAuthenticator authenticator;

    @Override
    public void onCreate () {
        this.authenticator = new AccountAuthenticator( this );
    }

    @Override
    public IBinder onBind ( Intent intent ) {
        return this.authenticator.getIBinder();
    }

    public static AccountManagerFuture< Bundle > getAccountToken (
            Activity context,
            AccountManagerCallback< Bundle > callback ) {

        AccountManager manager = AccountManager.get( App.getContext() );
        return manager.getAuthTokenByFeatures(
                Constants.ACCOUNT_TYPE,
                Constants.TOKEN_TYPE,
                null,
                context,
                Bundle.EMPTY,
                Bundle.EMPTY,
                callback,
                null
        );
    }

    public static AccountManagerFuture< Bundle > getAccountToken (
            String username,
            Activity context,
            AccountManagerCallback< Bundle > callback ) {

        AccountManager manager = AccountManager.get( App.getContext() );
        Account[] accounts = manager.getAccountsByType( Constants.ACCOUNT_TYPE );
        for ( Account account : accounts ) {
            if ( account.name.equals( username ) ) {
                return manager.getAuthToken(
                        account,
                        Constants.TOKEN_TYPE,
                        Bundle.EMPTY,
                        context,
                        callback,
                        null
                );
            }
        }

        return null;
    }

    public static void logout () {
        Context context = App.getContext();

        SharedPreferences prefs = context.getSharedPreferences( Constants.PREFS_APP, 0 );
        String token = prefs.getString( Constants.PREFS_APP_TOKEN, "" );

        if ( TextUtils.isEmpty( token ) ) {
            return;
        }

        AccountManager manager = AccountManager.get( context );
        manager.invalidateAuthToken( Constants.ACCOUNT_TYPE, token );
    }

    public static void setAuth ( String token ) {
        RestAPI.getInstance().setAuth( token );
    }
}

package papagaio.app.service;

import android.app.Application;

import com.google.gson.Gson;
import com.octo.android.robospice.persistence.CacheManager;
import com.octo.android.robospice.persistence.exception.CacheCreationException;
import com.octo.android.robospice.persistence.exception.CacheLoadingException;
import com.octo.android.robospice.persistence.exception.CacheSavingException;
import com.octo.android.robospice.persistence.file.InFileObjectPersister;
import com.octo.android.robospice.persistence.file.InFileObjectPersisterFactory;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;

public class SpiceService extends com.octo.android.robospice.SpiceService {
    @Override
    public CacheManager createCacheManager ( Application application ) throws CacheCreationException {
        CacheManager manager = new CacheManager();
        manager.addPersister( new ObjectPersisterFactory( application ) );

        return manager;
    }

    @Override
    public int getThreadCount () {
        return 3;
    }
}

class ObjectPersisterFactory extends InFileObjectPersisterFactory {

    public ObjectPersisterFactory ( Application application ) throws CacheCreationException {
        super( application );
    }

    @Override
    public < T > InFileObjectPersister< T > createInFileObjectPersister ( Class< T > clazz, File cacheFolder ) throws CacheCreationException {
        return new ObjectPersister<>( getApplication(), clazz, cacheFolder );
    }
}

class ObjectPersister< T > extends InFileObjectPersister< T > {

    private Gson gson = new Gson();

    public ObjectPersister ( Application application, Class< T > clazz ) throws CacheCreationException {
        super( application, clazz );
    }

    public ObjectPersister ( Application application, Class< T > clazz, File cacheFolder ) throws CacheCreationException {
        super( application, clazz, cacheFolder );
    }

    @Override
    protected T readCacheDataFromFile ( File file ) throws CacheLoadingException {
        Reader reader = null;

        try {
            reader = new FileReader( file );
            return gson.fromJson( reader, getHandledClass() );
        } catch ( FileNotFoundException e ) {
            return null;
        } catch ( Exception e ) {
            throw new CacheLoadingException( e );
        } finally {
            IOUtils.closeQuietly( reader );
        }
    }

    @Override
    public T saveDataToCacheAndReturnData ( T data, Object cacheKey ) throws CacheSavingException {
        Writer writer = null;
        try {
            writer = new FileWriter( getCacheFile( cacheKey ) );
            gson.toJson( data, writer );
        } catch ( Exception e ) {
            throw new CacheSavingException( e );
        } finally {
            IOUtils.closeQuietly( writer );
        }

        return data;
    }
}

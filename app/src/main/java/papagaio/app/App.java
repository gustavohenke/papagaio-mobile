package papagaio.app;

import android.app.Application;
import android.content.Context;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import de.greenrobot.event.EventBus;
import papagaio.app.util.Constants;

public class App extends Application {

    private static Context context;
    public final static EventBus bus = EventBus.getDefault();

    @Override
    public void onCreate () {
        super.onCreate();
        context = this;

        initImageLoader();

        //noinspection ResultOfMethodCallIgnored
        Constants.FILES_ROOT.mkdirs();
    }

    private void initImageLoader () {
        DisplayImageOptions.Builder displayOptions = new DisplayImageOptions.Builder();
        displayOptions.cacheInMemory( true );
        displayOptions.cacheOnDisk( true );

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder( this );
        config.memoryCacheSize( 2 * 1024 * 1024 );
        config.diskCacheSize( 20 * 1024 * 1024 );
        config.defaultDisplayImageOptions( displayOptions.build() );
        config.writeDebugLogs();
        ImageLoader.getInstance().init( config.build() );
    }

    public static Context getContext () {
        return context;
    }
}

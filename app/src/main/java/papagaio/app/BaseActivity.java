package papagaio.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import de.greenrobot.event.EventBusException;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        try {
            App.bus.register( this );
        } catch ( EventBusException e ) {}
    }

    @Override
    protected void onDestroy () {
        super.onDestroy();
        App.bus.unregister( this );
    }
}

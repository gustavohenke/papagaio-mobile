package papagaio.app.feature.timeline;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;
import papagaio.app.BaseActivity;
import papagaio.app.MainActivity;
import papagaio.app.R;
import papagaio.app.feature.newPost.NewPostActivity;
import papagaio.app.feature.search.SearchActivity;
import papagaio.app.fragment.LoadingFragment;
import papagaio.app.fragment.postList.PostListFactory;
import papagaio.app.fragment.postList.PostListFragment;
import papagaio.app.model.Listener;
import papagaio.app.model.post.CreatePostRequest;
import papagaio.app.model.post.Post;
import papagaio.app.service.AuthService;
import papagaio.app.util.exceptionHandler.SnackbarExceptionHandler;

public class TimelineActivity extends BaseActivity implements PostListFragment.OnReadyListener {

    private static final String TAG_LOADING = "loading";
    private static final String TAG_POST_LIST = "postList";
    private LoadingFragment loading;
    private PostListFragment postList;
    private View fab;

    // ---------------------------------------------------------------------------------------------
    // Activity
    // ---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_timeline );

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        if ( savedInstanceState == null ) {
            loading = new LoadingFragment();
            postList = PostListFragment.newInstance( getPostListFactory() );

            transaction
                    .add( R.id.timeline_fragment_container, loading, TAG_LOADING )
                    .add( R.id.timeline_fragment_container, postList, TAG_POST_LIST );
        } else {
            loading = ( LoadingFragment ) getFragmentManager().findFragmentByTag( TAG_LOADING );
            postList = ( PostListFragment ) getFragmentManager().findFragmentByTag( TAG_POST_LIST );
        }

        transaction.hide( postList ).commit();

        fab = findViewById( R.id.timeline_new_post );
        fab.setOnClickListener( new NewPostClickListener() );
    }

    // ---------------------------------------------------------------------------------------------
    // PostListFragment.OnReadyListener interface
    // ---------------------------------------------------------------------------------------------

    @Override
    public void onReady ( List< Post > posts, Exception exception ) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.hide( loading );
        transaction.show( postList );

        if ( exception != null ) {
            new SnackbarExceptionHandler( findViewById( android.R.id.content ) )
                    .handle( this, exception );
        }

        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu ( Menu menu ) {
        getMenuInflater().inflate( R.menu.timeline, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected ( MenuItem item ) {
        Intent intent;

        switch ( item.getItemId() ) {
            case R.id.timeline_menuitem_search:
                intent = new Intent( this, SearchActivity.class );
                startActivity( intent );
                return true;

            case R.id.timeline_menuitem_logout:
                AuthService.logout();
                intent = new Intent( this, MainActivity.class );
                startActivity( intent );
                finish();
                return true;
        }

        return super.onOptionsItemSelected( item );
    }

    // ---------------------------------------------------------------------------------------------
    // EventBus
    // ---------------------------------------------------------------------------------------------

    @Subscribe( threadMode = ThreadMode.MainThread )
    public void onEvent ( CreatePostRequest request ) {
        request.listen( new NewPostListener() );
    }

    // ---------------------------------------------------------------------------------------------
    // Outros métodos
    // ---------------------------------------------------------------------------------------------

    protected PostListFactory getPostListFactory () {
        return new TimelinePostListFactory();
    }

    // ---------------------------------------------------------------------------------------------

    class NewPostListener extends Listener< Post > {
        @Override
        public void success ( Post post ) {

        }

        @Override
        public void failure ( Exception exception ) {
            new SnackbarExceptionHandler( fab, Snackbar.LENGTH_LONG ).handle(
                    TimelineActivity.this,
                    exception
            );
        }
    }

    // ---------------------------------------------------------------------------------------------

    class NewPostClickListener implements View.OnClickListener {

        @Override
        public void onClick ( View v ) {
            Intent intent = new Intent( TimelineActivity.this, NewPostActivity.class );
            startActivity( intent );
        }
    }
}

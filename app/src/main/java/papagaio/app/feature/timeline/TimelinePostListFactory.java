package papagaio.app.feature.timeline;

import android.os.Parcel;

import papagaio.app.fragment.postList.PostListFactory;
import papagaio.app.model.Request;
import papagaio.app.model.post.ListPostRequest;
import papagaio.app.model.post.Post;

public class TimelinePostListFactory implements PostListFactory {

    public TimelinePostListFactory () {}

    @Override
    public Request< Post.List > createRequest () {
        return new ListPostRequest();
    }

    // ---------------------------------------------------------------------------------------------
    // Parcelable interface
    // ---------------------------------------------------------------------------------------------

    protected TimelinePostListFactory ( Parcel in ) {

    }

    @Override
    public int describeContents () {
        return 0;
    }

    @Override
    public void writeToParcel ( Parcel dest, int flags ) {

    }

    public static final Creator< TimelinePostListFactory > CREATOR = new Creator< TimelinePostListFactory >() {
        @Override
        public TimelinePostListFactory createFromParcel ( Parcel in ) {
            return new TimelinePostListFactory( in );
        }

        @Override
        public TimelinePostListFactory[] newArray ( int size ) {
            return new TimelinePostListFactory[ size ];
        }
    };
}

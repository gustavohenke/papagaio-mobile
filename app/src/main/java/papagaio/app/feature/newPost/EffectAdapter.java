package papagaio.app.feature.newPost;

import android.graphics.Bitmap;
import android.opengl.GLSurfaceView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TextView;

import papagaio.app.R;
import papagaio.app.adapter.ListRecyclerAdapter;
import papagaio.app.model.effect.EffectsRenderer;
import papagaio.app.model.effect.PhotoEffect;

public class EffectAdapter extends ListRecyclerAdapter< PhotoEffect, EffectAdapter.ViewHolder > {

    private OnClickListener onClickListener;
    private final Bitmap photo;
    private Bitmap previewPhoto;
    private int previewWidth;
    private int previewHeight;

    public EffectAdapter ( Bitmap photo ) {
        this.photo = photo;
    }

    @Override
    public ViewHolder onCreateViewHolder ( ViewGroup parent, int viewType ) {
        View view = LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.listitem_effect, parent, false );
        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder ( ViewHolder holder, int position ) {
        final PhotoEffect photoEffect = get( position );

        GlobalLayoutListener listener = new GlobalLayoutListener( holder, photoEffect );
        holder.view.getViewTreeObserver().addOnGlobalLayoutListener( listener );

        holder.textLabel.setText( photoEffect.getLabel() );

        if ( onClickListener != null ) {
            holder.view.setOnClickListener( new View.OnClickListener() {

                @Override
                public void onClick ( View v ) {
                    onClickListener.onClick( photoEffect );
                }
            });
        }
    }

    public OnClickListener getOnClickListener () {
        return onClickListener;
    }

    public void setOnClickListener ( OnClickListener onClickListener ) {
        this.onClickListener = onClickListener;
    }

    // ---------------------------------------------------------------------------------------------

    class GlobalLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {

        private final ViewHolder holder;
        private final PhotoEffect photoEffect;

        public GlobalLayoutListener ( ViewHolder holder, PhotoEffect photoEffect ) {
            this.holder = holder;
            this.photoEffect = photoEffect;
        }

        @Override
        public void onGlobalLayout () {
            if ( previewPhoto == null ) {
                final int oldWidth = photo.getWidth();
                final int oldHeight = photo.getHeight();
                final int targetWidth = holder.view.getWidth();
                final int targetHeight = holder.view.getHeight();
                final float ratio = Math.min(
                        ( float ) targetWidth / oldWidth,
                        ( float ) targetHeight / oldHeight
                );

                previewWidth = ( int ) Math.floor( oldWidth * ratio );
                previewHeight = ( int ) Math.floor( oldHeight * ratio );
                previewPhoto = Bitmap.createScaledBitmap( photo, previewWidth, previewHeight, true );
            }

            if ( holder.hasGLSurface() ) {
                holder.removeGLSurface();
            }

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams( previewWidth, previewHeight );
            EffectsRenderer renderer = new EffectsRenderer( previewPhoto );
            renderer.setEffect( photoEffect );

            GLSurfaceView glSurface = new GLSurfaceView( holder.view.getContext() );
            glSurface.setEGLContextClientVersion( 2 );
            glSurface.setRenderer( renderer );
            glSurface.setRenderMode( GLSurfaceView.RENDERMODE_WHEN_DIRTY );
            glSurface.setLayoutParams( params );
            holder.addGLSurface( glSurface );
            holder.view.getViewTreeObserver().removeOnGlobalLayoutListener( this );
        }
    }

    // ---------------------------------------------------------------------------------------------

    class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public final TextView textLabel;

        public ViewHolder ( View itemView ) {
            super( itemView );
            view = itemView;
            textLabel = ( TextView ) itemView.findViewById( R.id.effect_label );
        }

        public void addGLSurface ( GLSurfaceView surface ) {
            ( ( ViewGroup ) view ).addView( surface, 0 );
        }

        public void removeGLSurface () {
            if ( hasGLSurface() ) {
                ( ( ViewGroup ) view ).removeViewAt( 0 );
            }
        }

        public boolean hasGLSurface () {
            return ( ( ViewGroup ) view ).getChildAt( 0 ) instanceof GLSurfaceView;
        }
    }

    // ---------------------------------------------------------------------------------------------

    interface OnClickListener {

        void onClick ( PhotoEffect photoEffect );

    }

}

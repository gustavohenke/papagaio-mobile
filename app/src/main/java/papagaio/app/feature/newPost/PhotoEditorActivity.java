package papagaio.app.feature.newPost;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.FileOutputStream;
import java.io.IOException;

import papagaio.app.BaseActivity;
import papagaio.app.R;
import papagaio.app.model.effect.AutoFixEffect;
import papagaio.app.model.effect.BrightnessEffect;
import papagaio.app.model.effect.ContrastEffect;
import papagaio.app.model.effect.EffectsRenderer;
import papagaio.app.model.effect.FishEyeEffect;
import papagaio.app.model.effect.GrayscaleEffect;
import papagaio.app.model.effect.NoEffect;
import papagaio.app.model.effect.PhotoEffect;
import papagaio.app.model.effect.PosterizeEffect;
import papagaio.app.model.effect.SepiaEffect;
import papagaio.app.model.effect.VignetteEffect;
import papagaio.app.util.file.DefaultTempFileManager;

public class PhotoEditorActivity extends BaseActivity {

    public static Intent newIntent ( Context context, Uri uri ) {
        Intent intent = new Intent( context, PhotoEditorActivity.class );
        intent.putExtra( EXTRA_URI, uri );
        return intent;
    }

    // ---------------------------------------------------------------------------------------------

    private static final String EXTRA_URI = "uri";
    private RelativeLayout layoutContainer;
    private RecyclerView listFilters;
    private GLSurfaceView glSurface;
    private EffectsRenderer renderer;
    private Uri uri;

    // ---------------------------------------------------------------------------------------------
    // Activity
    // ---------------------------------------------------------------------------------------------

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        requestWindowFeature( Window.FEATURE_ACTION_BAR_OVERLAY );

        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_photo_editor );

        Toolbar toolbar = ( Toolbar ) findViewById( R.id.photoeditor_toolbar );
        setSupportActionBar( toolbar );

        layoutContainer = ( RelativeLayout ) findViewById( R.id.photoeditor_container );
        listFilters = ( RecyclerView ) findViewById( R.id.photoeditor_filters );

        uri = getIntent().getParcelableExtra( EXTRA_URI );
        ImageLoader.getInstance().loadImage( uri.toString(), new ImageLoadingListener() {
            @Override
            public void onLoadingStarted ( String imageUri, View view ) {

            }

            @Override
            public void onLoadingFailed ( String imageUri, View view, FailReason failReason ) {

            }

            @Override
            public void onLoadingComplete ( String imageUri, View view, Bitmap loadedImage ) {
                View loading = findViewById( R.id.photoeditor_loading );
                ( ( ViewGroup ) loading.getParent() ).removeView( loading );

                initFilterList( loadedImage );
                initPreview( loadedImage );
            }

            @Override
            public void onLoadingCancelled ( String imageUri, View view ) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu ( Menu menu ) {
        getMenuInflater().inflate( R.menu.photo_editor, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected ( MenuItem item ) {
        renderer.setOnBitmapGeneratedListener( new EffectsRenderer.OnBitmapGeneratedListener() {
            @Override
            public void onBitmapGenerated ( Bitmap bitmap ) {
                renderer.setOnBitmapGeneratedListener( null );

                try {
                    DefaultTempFileManager tempFileManager = new DefaultTempFileManager();
                    FileOutputStream fos = new FileOutputStream( tempFileManager.next().getAbsolutePath() );
                    bitmap.compress( Bitmap.CompressFormat.JPEG, 100, fos );

                    fos.flush();
                    fos.close();

                    Intent data = new Intent();
                    data.setData( Uri.fromFile( tempFileManager.last() ) );
                    setResult( Activity.RESULT_OK, data );
                    finish();
                } catch ( IOException ignored ) {}
            }
        });

        glSurface.requestRender();
        return super.onOptionsItemSelected( item );
    }

    // ---------------------------------------------------------------------------------------------
    // Outros métodos
    // ---------------------------------------------------------------------------------------------

    private void initFilterList ( Bitmap photo ) {
        EffectAdapter adapter = new EffectAdapter( photo );
        adapter.add( new NoEffect() );
        adapter.add( new AutoFixEffect() );
        adapter.add( new BrightnessEffect() );
        adapter.add( new ContrastEffect() );
        adapter.add( new FishEyeEffect() );
        adapter.add( new GrayscaleEffect() );
        adapter.add( new PosterizeEffect() );
        adapter.add( new SepiaEffect() );
        adapter.add( new VignetteEffect() );

        adapter.setOnClickListener( new ApplyEffectClickListener() );

        listFilters.setLayoutManager( new LinearLayoutManager(
                PhotoEditorActivity.this,
                LinearLayoutManager.HORIZONTAL,
                false
        ));
        listFilters.setAdapter( adapter );
    }

    private void initPreview ( Bitmap original ) {
        final int oldWidth = original.getWidth();
        final int oldHeight = original.getHeight();
        final int targetWidth = layoutContainer.getWidth();
        final int targetHeight = layoutContainer.getHeight();
        final float ratio = Math.min(
                ( float ) targetWidth / oldWidth,
                ( float ) targetHeight / oldHeight
        );

        final int newWidth = ( int ) Math.floor( oldWidth * ratio );
        final int newHeight = ( int ) Math.floor( oldHeight * ratio );

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams( newWidth, newHeight );
        params.addRule( RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE );

        Bitmap photo = Bitmap.createScaledBitmap( original, newWidth, newHeight, true );

        renderer = new EffectsRenderer( photo );

        glSurface = new GLSurfaceView( PhotoEditorActivity.this );
        glSurface.setLayoutParams( params );
        glSurface.setEGLContextClientVersion( 2 );
        glSurface.setRenderer( renderer );
        glSurface.setRenderMode( GLSurfaceView.RENDERMODE_WHEN_DIRTY );

        // Adicionando ao indice 0, garante que a foto estará na parte superior do container
        layoutContainer.addView( glSurface, 0 );
    }

    // ---------------------------------------------------------------------------------------------

    class ApplyEffectClickListener implements EffectAdapter.OnClickListener {

        @Override
        public void onClick ( PhotoEffect photoEffect ) {
            renderer.setEffect( photoEffect );
            glSurface.requestRender();
        }
    }
}

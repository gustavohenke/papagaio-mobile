package papagaio.app.feature.newPost;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import papagaio.app.BaseActivity;
import papagaio.app.R;
import papagaio.app.model.Listener;
import papagaio.app.model.location.GetLocationRequest;
import papagaio.app.model.location.Location;
import papagaio.app.model.post.CreatePostRequest;
import papagaio.app.model.post.Post;
import papagaio.app.util.Constants;
import papagaio.app.util.IconToggler;
import papagaio.app.util.exceptionHandler.DialogExceptionHandler;
import papagaio.app.util.file.DefaultTempFileManager;
import papagaio.app.util.intent.IntentFactory;
import papagaio.app.util.intent.SelectPhotoIntentFactory;

public class NewPostActivity extends BaseActivity implements View.OnKeyListener {

    private final static int INTENT_PHOTO = 1;
    private final static int INTENT_EDIT = 2;

    private EditText editText;
    private TextView textCount;
    private TextView textLocation;
    private ImageView iconPhoto;
    private ImageView iconLocation;
    private ImageView imagePhoto;
    private Uri photo;
    private Location location;
    private IconToggler iconToggler;
    private final DefaultTempFileManager tempFileManager = new DefaultTempFileManager();

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_new_post );

        iconToggler = new IconToggler( this );

        editText = ( EditText ) findViewById( R.id.newpost_edit_text );
        editText.setOnKeyListener( this );

        InputFilter[] filters = new InputFilter[]{
                new InputFilter.LengthFilter( Constants.MAX_CHARS )
        };
        editText.setFilters( filters );

        textCount = ( TextView ) findViewById( R.id.newpost_text_count );
        textLocation = ( TextView ) findViewById( R.id.newpost_text_location );

        TextView textMaxChars = ( TextView ) findViewById( R.id.newpost_text_maxchars );
        textMaxChars.setText( getString( R.string.newpost_max_chars, Constants.MAX_CHARS ) );

        imagePhoto = ( ImageView ) findViewById( R.id.newpost_image_photo );
        iconPhoto = ( ImageView ) findViewById( R.id.newpost_icon_photo );
        iconLocation = ( ImageView ) findViewById( R.id.newpost_icon_location );

        // O click listener tem que ser adicionado no parent, ou senão o efeito ripple não será
        // ativado.
        ( ( View ) iconPhoto.getParent() ).setOnClickListener( new SelectPhotoListener() );
        ( ( View ) iconLocation.getParent() ).setOnClickListener( new TriggerLocationListener() );

        updateCharCounter();
        togglePhotoStatus();
        toggleLocationStatus();
    }

    @Override
    public boolean onCreateOptionsMenu ( Menu menu ) {
        getMenuInflater().inflate( R.menu.new_post, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected ( MenuItem item ) {
        if ( item.getItemId() != R.id.newpost_menuitem_finish ) {
            return super.onOptionsItemSelected( item );
        }

        Post post = new Post();
        post.setText( editText.getText().toString() );
        post.setLocation( location );

        CreatePostRequest createPost = new CreatePostRequest( post );

        if ( photo != null ) {
            ContentResolver contentResolver = getContentResolver();
            contentResolver.notifyChange( photo, null );

            createPost.setPhoto( contentResolver, photo );
        }

        createPost.execute();
        finish();

        return super.onOptionsItemSelected( item );
    }

    @Override
    public boolean onKey ( View v, int keyCode, KeyEvent event ) {
        updateCharCounter();
        return false;
    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data ) {
        super.onActivityResult( requestCode, resultCode, data );
        switch ( requestCode ) {
            case INTENT_PHOTO:
                handlePhoto( resultCode, data );
                break;

            case INTENT_EDIT:
                handleEditedPhoto( resultCode, data );
                break;
        }
    }

    @Override
    protected void onDestroy () {
        super.onDestroy();

        if ( isFinishing() ) {
            tempFileManager.destroy();
        }
    }

    // ---------------------------------------------------------------------------------------------

    private void updateCharCounter () {
        int length = editText.getText().length();
        textCount.setText( getString( R.string.newpost_text_length, length ) );
    }

    private void togglePhotoStatus () {
        iconToggler.toggle( photo != null, iconPhoto );
    }

    private void toggleLocationStatus () {
        iconToggler.toggle( location != null, iconLocation );
        if ( location != null ) {
            textLocation.setText( getString(
                    R.string.newpost_in_location,
                    location.toString()
            ));
        }
    }

    private void handlePhoto ( int resultCode, Intent data ) {
        if ( resultCode != RESULT_OK ) {
            return;
        }

        boolean isCamera = data == null || TextUtils.equals( data.getAction(), MediaStore.ACTION_IMAGE_CAPTURE );
        Uri uri = isCamera ? Uri.fromFile( tempFileManager.last() ) : data.getData();

        Intent intent = PhotoEditorActivity.newIntent( this, uri );
        startActivityForResult( intent, INTENT_EDIT );
    }

    private void handleEditedPhoto ( int resultCode, Intent data ) {
        if ( resultCode != RESULT_OK ) {
            return;
        }

        Uri uri = data.getData();

        photo = uri;
        imagePhoto.setImageURI( uri );
        togglePhotoStatus();
    }

    // ---------------------------------------------------------------------------------------------

    private class SelectPhotoListener implements View.OnClickListener {

        @Override
        public void onClick ( View v ) {
            IntentFactory factory = new SelectPhotoIntentFactory( tempFileManager );
            Intent intent = factory.create( NewPostActivity.this );
            startActivityForResult( intent, INTENT_PHOTO );
        }
    }

    // ---------------------------------------------------------------------------------------------

    private class TriggerLocationListener implements View.OnClickListener {

        @Override
        public void onClick ( View v ) {
            GetLocationRequest request = new GetLocationRequest();
            request.execute( new GetLocationRequestListener() );
        }
    }

    // ---------------------------------------------------------------------------------------------

    private class GetLocationRequestListener extends Listener< Location > {

        @Override
        public void success ( Location location ) {
            NewPostActivity.this.location = location;
            toggleLocationStatus();
        }

        @Override
        public void failure ( Exception exception ) {
            new DialogExceptionHandler().handle( NewPostActivity.this, exception );
        }
    }
}

package papagaio.app.feature.login;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import papagaio.app.R;
import papagaio.app.model.user.User;

public class NewAccountFragment extends Fragment {

    private OnSubmitListener onSubmitListener;
    private EditText editUsername;
    private EditText editPassword;
    private EditText editEmail;

    public NewAccountFragment () {}

    @Nullable
    @Override
    public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        return inflater.inflate( R.layout.fragment_new_account, container, false );
    }

    @Override
    public void onViewCreated ( View view, Bundle savedInstanceState ) {
        super.onViewCreated( view, savedInstanceState );

        editUsername = ( EditText ) view.findViewById( R.id.newaccount_edit_username );
        editEmail = ( EditText ) view.findViewById( R.id.newaccount_edit_email );
        editPassword = ( EditText ) view.findViewById( R.id.newaccount_edit_password );

        view.findViewById( R.id.newaccount_button_submit ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick ( View v ) {
                User user = new User();
                user.setUsername( editUsername.getText().toString() );
                user.setEmail( editEmail.getText().toString() );
                user.setPassword( editPassword.getText().toString() );

                if ( onSubmitListener != null ) {
                    onSubmitListener.onSubmit( user );
                }
            }
        });
    }

    // ---------------------------------------------------------------------------------------------
    // Getters & setters
    // ---------------------------------------------------------------------------------------------

    public OnSubmitListener getOnSubmitListener () {
        return onSubmitListener;
    }

    public void setOnSubmitListener ( OnSubmitListener onSubmitListener ) {
        this.onSubmitListener = onSubmitListener;
    }

    // ---------------------------------------------------------------------------------------------

    public interface OnSubmitListener {
        void onSubmit ( User user );
    }

}

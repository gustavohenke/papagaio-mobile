package papagaio.app.feature.login;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import papagaio.app.R;
import papagaio.app.util.LaunchIntentClickListener;

public class LoginFragment extends Fragment {

    private OnSubmit mListener;

    public LoginFragment () {}

    @Override
    public View onCreateView ( LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState ) {
        return inflater.inflate( R.layout.fragment_login, container, false );
    }

    @Override
    public void onViewCreated ( View view, Bundle savedInstanceState ) {
        super.onViewCreated( view, savedInstanceState );

        final EditText editUsername = ( EditText ) view.findViewById( R.id.login_edit_username );
        final EditText editPassword = ( EditText ) view.findViewById( R.id.login_edit_password );

        view.findViewById( R.id.login_button_submit ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick ( View v ) {
                mListener.submit(
                        editUsername.getText().toString(),
                        editPassword.getText().toString()
                );
            }
        });

        Intent intent = new Intent( getActivity(), NewAccountActivity.class );
        View.OnClickListener listener = new LaunchIntentClickListener( intent );
        view.findViewById( R.id.login_button_new_account ).setOnClickListener( listener );
    }

    @Override
    public void onAttach ( Activity activity ) {
        super.onAttach( activity );
        try {
            mListener = ( OnSubmit ) activity;
        } catch ( ClassCastException e ) {
            throw new ClassCastException( activity.toString()
                    + " must implement OnSubmit" );
        }
    }

    @Override
    public void onDetach () {
        super.onDetach();
        mListener = null;
    }

    public interface OnSubmit {
        void submit ( String username, String password );
    }

}
package papagaio.app.feature.login;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.EditText;

import papagaio.app.BaseActivity;
import papagaio.app.R;
import papagaio.app.fragment.LoadingFragment;
import papagaio.app.model.Listener;
import papagaio.app.model.user.CreateUserRequest;
import papagaio.app.model.user.User;
import papagaio.app.util.exceptionHandler.DialogExceptionHandler;

public class NewAccountActivity extends BaseActivity implements NewAccountFragment.OnSubmitListener {

    private LoadingFragment loadingFragment;
    private NewAccountFragment newAccountFragment;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        requestWindowFeature( Window.FEATURE_ACTION_BAR_OVERLAY );

        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_new_account );

        Toolbar toolbar = ( Toolbar ) findViewById( R.id.newaccount_toolbar );
        setSupportActionBar( toolbar );

        loadingFragment = new LoadingFragment();
        newAccountFragment = new NewAccountFragment();
        newAccountFragment.setOnSubmitListener( this );

        getFragmentManager().beginTransaction()
                .add( R.id.newaccount_fragment_container, loadingFragment )
                .add( R.id.newaccount_fragment_container, newAccountFragment )
                .hide( loadingFragment )
                .commit();
    }

    @Override
    public void onSubmit ( User user ) {
        getFragmentManager().beginTransaction()
                .hide( newAccountFragment )
                .show( loadingFragment )
                .commit();

        CreateUserRequest request = new CreateUserRequest( user );
        request.execute( new CreateUserListener() );
    }

    // ---------------------------------------------------------------------------------------------

    class CreateUserListener extends Listener< User > {

        @Override
        public void always ( User user, Exception exception ) {
            super.always( user, exception );
            getFragmentManager().beginTransaction()
                    .hide( loadingFragment )
                    .show( newAccountFragment )
                    .commit();
        }

        @Override
        public void success ( User user ) {
            finish();
        }

        @Override
        public void failure ( Exception exception ) {
            new DialogExceptionHandler().handle( NewAccountActivity.this, exception );
        }
    }
}

package papagaio.app.feature.login;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import papagaio.app.R;
import papagaio.app.fragment.LoadingFragment;
import papagaio.app.model.Listener;
import papagaio.app.model.session.Credentials;
import papagaio.app.model.session.LoginRequest;
import papagaio.app.util.Constants;
import papagaio.app.util.exceptionHandler.DialogExceptionHandler;

public class LoginActivity extends AccountAuthenticatorActivity implements LoginFragment.OnSubmit {

    private AccountManager accountManager;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );

        this.accountManager = AccountManager.get( this );
        this.setFragment( new LoginFragment() );
    }

    @Override
    public void submit ( String username, String password ) {
        final Fragment oldFragment = getFragment();
        final Credentials credentials = new Credentials( username, password );
        setFragment( new LoadingFragment() );

        new LoginRequest( credentials ).execute( new Listener< String >() {
            @Override
            public void always ( String s, Exception exception ) {
                super.always( s, exception );
                setFragment( oldFragment );
            }

            @Override
            public void success ( String token ) {
                if ( token == null ) {
                    new AlertDialog.Builder( LoginActivity.this )
                            .setTitle( R.string.login_error_title )
                            .setMessage( R.string.login_error_description )
                            .show();
                    return;
                }

                Account account = new Account(
                        credentials.getUsername(),
                        Constants.ACCOUNT_TYPE
                );
                accountManager.addAccountExplicitly( account, credentials.getPassword(), null );
                accountManager.setAuthToken( account, Constants.TOKEN_TYPE, token );

                // Montamos o intent necessário para devolver o resultado ao Authenticator...
                Intent intent = new Intent();
                intent.putExtra( AccountManager.KEY_ACCOUNT_NAME, account.name );
                intent.putExtra( AccountManager.KEY_ACCOUNT_TYPE, account.type );
                intent.putExtra( AccountManager.KEY_PASSWORD, credentials.getPassword() );
                intent.putExtra( AccountManager.KEY_AUTHTOKEN, token );
                intent.putExtra( AccountManager.KEY_BOOLEAN_RESULT, true );

                setAccountAuthenticatorResult( intent.getExtras() );
                setResult( RESULT_OK, intent );
                finish();
            }

            @Override
            public void failure ( Exception exception ) {
                new DialogExceptionHandler().handle( LoginActivity.this, exception );
            }
        });
    }

    private void setFragment ( Fragment fragment ) {
        getFragmentManager().beginTransaction()
                .replace( R.id.login_fragment_container, fragment )
                .commit();
    }

    private Fragment getFragment () {
        return getFragmentManager().findFragmentById( R.id.login_fragment_container );
    }
}

package papagaio.app.feature.profile;

import android.os.Parcel;

import papagaio.app.fragment.postList.PostListFactory;
import papagaio.app.model.Request;
import papagaio.app.model.post.ListPostRequest;
import papagaio.app.model.post.ListUserPostRequest;
import papagaio.app.model.post.Post;

public class ProfilePostListFactory implements PostListFactory {

    private final int userId;
    private final String username;

    public ProfilePostListFactory ( int userId ) {
        this.userId = userId;
        this.username = null;
    }

    public ProfilePostListFactory ( String username ) {
        this.userId = 0;
        this.username = username;
    }

    @Override
    public Request< Post.List > createRequest () {
        return username != null ?
                new ListUserPostRequest( username ) :
                new ListUserPostRequest( userId );
    }

    // ---------------------------------------------------------------------------------------------
    // Parcelable interface
    // ---------------------------------------------------------------------------------------------

    protected ProfilePostListFactory ( Parcel in ) {
        this.userId = in.readInt();
        this.username = in.readString();
    }

    @Override
    public int describeContents () {
        return 0;
    }

    @Override
    public void writeToParcel ( Parcel dest, int flags ) {
        dest.writeInt( userId );
        dest.writeString( username );
    }

    public static final Creator< ProfilePostListFactory > CREATOR = new Creator< ProfilePostListFactory >() {
        @Override
        public ProfilePostListFactory createFromParcel ( Parcel in ) {
            return new ProfilePostListFactory( in );
        }

        @Override
        public ProfilePostListFactory[] newArray ( int size ) {
            return new ProfilePostListFactory[ size ];
        }
    };
}

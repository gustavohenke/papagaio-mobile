package papagaio.app.feature.profile;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Size;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.DateFormat;
import java.util.List;

import papagaio.app.BaseActivity;
import papagaio.app.R;
import papagaio.app.fragment.postList.PostListFragment;
import papagaio.app.model.Listener;
import papagaio.app.model.Request;
import papagaio.app.model.follow.FollowRequest;
import papagaio.app.model.follow.IsFollowingRequest;
import papagaio.app.model.follow.UnfollowRequest;
import papagaio.app.model.misc.Image;
import papagaio.app.model.post.Post;
import papagaio.app.model.user.GetUserRequest;
import papagaio.app.model.user.User;
import papagaio.app.util.Utils;

public class ProfileActivity extends BaseActivity
        implements PostListFragment.OnReadyListener, AppBarLayout.OnOffsetChangedListener {

    public static Intent newIntent ( Context context, int userId ) {
        Intent intent = new Intent( context, ProfileActivity.class );
        intent.putExtra( EXTRA_USER_ID, userId );

        return intent;
    }

    // ---------------------------------------------------------------------------------------------

    private static final String EXTRA_USER_ID = "userId";
    private static final int PERCENTAGE_ANIMATE_AVATAR = 15;
    private static final String TAG_POST_LIST = "postList";

    private PostListFragment postList;
    private String username;
    private int userId;
    private User user;
    private boolean isFollowing = false;

    private AppBarLayout appbar;
    private CollapsingToolbarLayout toolbarContainer;
    private Toolbar toolbar;
    private LinearLayout layoutSummary;
    private LinearLayout layoutFollowContainer;
    private TextView textRegistrationDate;
    private TextView textFollowingCount;
    private TextView textFollowersCount;
    private TextView textPostsCount;
    private ImageView imageAvatar;
    private Button buttonFollow;
    private int maxScrollSize;
    private boolean avatarShown = true;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_profile );

        final Intent intent = getIntent();

        if ( Intent.ACTION_VIEW.equals( intent.getAction() ) ) {
            final Uri data = intent.getData();
            username = data.getPathSegments().get( 0 );
        } else {
            userId = getIntent().getIntExtra( EXTRA_USER_ID, 0 );
        }

        // -----------------------------------------------------------------------------------------

        toolbar = ( Toolbar ) findViewById( R.id.profile_toolbar );
        setSupportActionBar( toolbar );

        appbar = ( AppBarLayout ) findViewById( R.id.profile_appbar );
        toolbarContainer = ( CollapsingToolbarLayout ) findViewById( R.id.profile_toolbar_container );
        layoutSummary = ( LinearLayout ) findViewById( R.id.profile_summary );
        layoutFollowContainer = ( LinearLayout ) findViewById( R.id.profile_follow_container );
        textRegistrationDate = ( TextView ) findViewById( R.id.profile_registration_date );
        textFollowingCount = ( TextView ) findViewById( R.id.profile_following_count );
        textFollowersCount = ( TextView ) findViewById( R.id.profile_followers_count );
        textPostsCount = ( TextView ) findViewById( R.id.profile_post_count );
        imageAvatar = ( ImageView ) findViewById( R.id.profile_avatar );
        buttonFollow = ( Button ) findViewById( R.id.profile_follow );

        buttonFollow.setOnClickListener( new ToggleFollowClickListener() );

        appbar.setVisibility( View.GONE );
        appbar.addOnOffsetChangedListener( ProfileActivity.this );
        maxScrollSize = appbar.getTotalScrollRange();

        // -----------------------------------------------------------------------------------------

        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        if ( savedInstanceState == null ) {
            ProfilePostListFactory factory = username != null ?
                    new ProfilePostListFactory( username ) :
                    new ProfilePostListFactory( userId );

            postList = PostListFragment.newInstance( factory );
            transaction.add( R.id.timeline_fragment_container, postList, TAG_POST_LIST );
        } else {
            postList = ( PostListFragment ) getFragmentManager().findFragmentByTag( TAG_POST_LIST );
        }

        transaction.hide( postList ).commit();

        // -----------------------------------------------------------------------------------------

        GetUserRequest request = username != null ?
                new GetUserRequest( username ) :
                new GetUserRequest( userId );
        request.execute( new GetUserListener() );
    }

    @Override
    protected void onResume () {
        super.onResume();

        TypedArray titleStyles = obtainStyledAttributes( R.style.ProfileTitle_Expanded, new int[] {
                android.R.attr.textSize
        });

        int avatarSummaryHeight = getResources().getDimensionPixelSize( R.dimen.profile_avatar_summary_height );
        int titleMarginTop = getResources().getDimensionPixelSize( R.dimen.profile_title_summary_margin_top );
        int titleTextSize = titleStyles.getDimensionPixelSize( 0, 0 );
        int summaryPaddingTop = titleMarginTop + titleTextSize + avatarSummaryHeight;

        layoutSummary.setPadding( 0, summaryPaddingTop, 0, 0 );
        layoutSummary.getViewTreeObserver()
                .addOnGlobalLayoutListener( new SummaryLayoutListener() );

        titleStyles.recycle();
    }

    // ---------------------------------------------------------------------------------------------
    // PostListFragment.OnReadyListener interface
    // ---------------------------------------------------------------------------------------------

    @Override
    public void onReady ( List< Post > posts, Exception exception ) {
        textPostsCount.setText( getString(
                R.string.profile_posts,
                posts.size()
        ) );
    }

    // ---------------------------------------------------------------------------------------------
    // AppBarLayout.OnOffsetChangedListener interface
    // ---------------------------------------------------------------------------------------------

    @Override
    public void onOffsetChanged ( AppBarLayout appbar, int verticalOffset ) {
        if ( maxScrollSize == 0 ) {
            maxScrollSize = appbar.getTotalScrollRange();
        }

        int percentage = Math.abs( verticalOffset ) * 100 / maxScrollSize;
        percentage = Math.min( percentage, 100 );

        if ( percentage >= PERCENTAGE_ANIMATE_AVATAR && avatarShown ) {
            avatarShown = false;
            imageAvatar.animate().scaleY( 0 ).scaleX( 0 ).setDuration( 200 ).start();
        }

        if ( percentage <= PERCENTAGE_ANIMATE_AVATAR && !avatarShown ) {
            avatarShown = true;
            imageAvatar.animate().scaleY( 1 ).scaleX( 1 ).start();
        }
    }

    // ---------------------------------------------------------------------------------------------

    private void applyTint ( TextView textView ) {
        Drawable[] drawables = textView.getCompoundDrawables();
        for ( Drawable drawable : drawables ) {
            if ( drawable != null ) {
                drawable.setTint( getResources().getColor( R.color.type_minor ) );
            }
        }
    }

    private void toggleFollow () {
        buttonFollow.setText( isFollowing ? R.string.profile_unfollow : R.string.profile_follow );
    }

    // ---------------------------------------------------------------------------------------------

    class SummaryLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {

        @Override
        public void onGlobalLayout () {
            final int summaryHeightId = R.dimen.profile_avatar_summary_height;
            final int avatarTopMarginId = R.dimen.profile_avatar_top_margin;
            int avatarSummaryHeight = getResources().getDimensionPixelSize( summaryHeightId );
            int avatarTopMargin = getResources().getDimensionPixelSize( avatarTopMarginId );

            CollapsingToolbarLayout.MarginLayoutParams avatarParams =
                    ( CollapsingToolbarLayout.MarginLayoutParams ) imageAvatar.getLayoutParams();

            avatarParams.setMargins(
                    avatarParams.leftMargin,
                    avatarParams.topMargin,
                    avatarParams.rightMargin,
                    layoutSummary.getHeight() - avatarSummaryHeight
            );
            imageAvatar.requestLayout();

            appbar.getLayoutParams().height  =
                    layoutSummary.getHeight() +
                    ( avatarParams.height - avatarSummaryHeight ) +
                    avatarTopMargin +
                    toolbar.getHeight();
            appbar.requestLayout();
        }
    }

    // ---------------------------------------------------------------------------------------------

    class GetUserListener extends Listener< User > {

        @Override
        public void success ( User user ) {
            ProfileActivity.this.user = user;

            getFragmentManager().beginTransaction().show( postList ).commit();

            View loading = findViewById( R.id.profile_loading );
            ( ( ViewGroup ) loading.getParent() ).removeView( loading );

            appbar.setVisibility( View.VISIBLE );

            applyTint( textFollowersCount );
            applyTint( textFollowingCount );
            applyTint( textPostsCount );
            applyTint( textRegistrationDate );

            // -------------------------------------------------------------------------------------

            if ( !Utils.getLoggedUsername().equals( user.getUsername() ) ) {
                IsFollowingRequest request = new IsFollowingRequest( user.getId() );
                request.execute( new IsFollowingListener() );
            }

            // -------------------------------------------------------------------------------------

            final int avatarSizeId = R.dimen.profile_avatar_size;
            final int avatarSize = getResources().getDimensionPixelSize( avatarSizeId );
            final Size size = new Size( avatarSize, avatarSize );

            Image photo = user.getPhoto();
            ImageLoader.getInstance().displayImage( photo.getUrl( size ), imageAvatar );

            toolbarContainer.setTitle( user.getStandardUsername() );

            DateFormat dateFormat = DateFormat.getDateInstance();
            textRegistrationDate.setText( dateFormat.format( user.getCreatedAt() ) );

            textFollowingCount.setText( getString(
                    R.string.profile_following,
                    user.getFollowingCount()
            ));
            textFollowersCount.setText( getString(
                    R.string.profile_followers,
                    user.getFollowersCount()
            ));
        }

        @Override
        public void failure ( Exception exception ) {

        }
    }

    // ---------------------------------------------------------------------------------------------

    class IsFollowingListener extends Listener< Void > {

        @Override
        public void always ( Void result, Exception exception ) {
            super.always( result, exception );

            isFollowing = exception == null;

            layoutFollowContainer.setVisibility( View.VISIBLE );
            toggleFollow();
        }

        @Override
        public void success ( Void result ) {}

        @Override
        public void failure ( Exception exception ) {}
    }

    // ---------------------------------------------------------------------------------------------

    class ToggleFollowClickListener implements View.OnClickListener {

        @Override
        public void onClick ( View v ) {
            Request< Void > request = isFollowing ?
                    new UnfollowRequest( user.getUsername() ) :
                    new FollowRequest( user.getUsername() );

            request.execute();

            isFollowing = !isFollowing;
            toggleFollow();
        }
    }
}

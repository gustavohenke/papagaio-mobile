package papagaio.app.feature.search;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import papagaio.app.R;
import papagaio.app.model.Listener;
import papagaio.app.model.user.ListUserRequest;
import papagaio.app.model.user.User;
import papagaio.app.util.Debouncer;

public class SearchResultFragment extends Fragment {

    private final long DELAY = 100;
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private final Debouncer debouncer = new Debouncer( 200 );
    private SearchResultAdapter adapter;
    private RecyclerView list;
    private OnResultListener onResultListener;

    // ---------------------------------------------------------------------------------------------
    // Fragment
    // ---------------------------------------------------------------------------------------------

    @Nullable
    @Override
    public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        return inflater.inflate( R.layout.list, container, false );
    }

    @Override
    public void onViewCreated ( View view, Bundle savedInstanceState ) {
        super.onViewCreated( view, savedInstanceState );

        adapter = new SearchResultAdapter( getActivity() );

        list = ( RecyclerView ) view.findViewById( android.R.id.list );
        list.setLayoutManager( new LinearLayoutManager( getActivity() ) );
        list.setAdapter( adapter );
    }

    @Override
    public void onDestroy () {
        super.onDestroy();
        debouncer.terminate();
        scheduler.shutdown();
    }

    // ---------------------------------------------------------------------------------------------

    public void setOnResultListener ( OnResultListener onResultListener ) {
        this.onResultListener = onResultListener;
    }

    public void query ( final String query ) {
        int size = adapter.size();
        if ( size > 0 ) {
            adapter.clear();
            adapter.notifyItemRangeRemoved( 0, size );
        }

        ListUserRequest searchUserRequest = new ListUserRequest( query );
        searchUserRequest.execute( new ListUserListener() );
    }

    private void update () {
        debouncer.call( new Runnable() {
            @Override
            public void run () {
                adapter.notifyDataSetChanged();

                if ( onResultListener != null ) {
                    onResultListener.onResult( adapter.getAll() );
                }
            }
        });
    }

    // ---------------------------------------------------------------------------------------------

    interface OnResultListener {
        void onResult ( List< SearchResult > results );
    }

    // ---------------------------------------------------------------------------------------------

    class ListUserListener extends Listener< User.List > {

        @Override
        public void success ( final User.List users ) {
            Runnable addResults = new Runnable() {
                @Override
                public void run () {
                    for ( User user : users ) {
                        adapter.add( new UserSearchResult( user ) );
                    }

                    update();
                }
            };
            scheduler.schedule( addResults, DELAY * 2, TimeUnit.MILLISECONDS );
        }

        @Override
        public void failure ( Exception exception ) {

        }
    }
}
package papagaio.app.feature.search;

import papagaio.app.model.user.User;
import papagaio.app.util.intent.IntentFactory;
import papagaio.app.util.intent.ProfileIntentFactory;

public class UserSearchResult implements SearchResult {
    private final User user;

    public UserSearchResult ( User user ) {
        this.user = user;
    }

    @Override
    public String getTitle () {
        return user.getStandardUsername();
    }

    @Override
    public IntentFactory getIntentFactory () {
        return new ProfileIntentFactory( user.getId() );
    }

}

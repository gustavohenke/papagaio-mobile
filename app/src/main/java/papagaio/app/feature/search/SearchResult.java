package papagaio.app.feature.search;

import papagaio.app.util.intent.IntentFactory;

public interface SearchResult {

    String getTitle();
    IntentFactory getIntentFactory();

}

package papagaio.app.feature.search;

import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import java.util.List;

import papagaio.app.BaseActivity;
import papagaio.app.R;
import papagaio.app.fragment.LoadingFragment;

public class SearchActivity extends BaseActivity
        implements SearchView.OnQueryTextListener, SearchResultFragment.OnResultListener {

    private SearchView search;
    private SearchResultFragment searchResult;
    private LoadingFragment loading;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_search );

        Toolbar toolbar = ( Toolbar ) findViewById( R.id.search_toolbar );
        setSupportActionBar( toolbar );

        loading = new LoadingFragment();
        searchResult = new SearchResultFragment();
        searchResult.setOnResultListener( this );

        getFragmentManager().beginTransaction()
                .add( R.id.search_fragment_container, searchResult )
                .add( R.id.search_fragment_container, loading )
                .hide( loading )
                .commit();

        search = ( SearchView ) findViewById( R.id.search );
        search.requestFocus();
        search.setOnQueryTextListener( this );
    }

    // ---------------------------------------------------------------------------------------------
    // SearchView.OnQueryTextListener interface
    // ---------------------------------------------------------------------------------------------

    @Override
    public boolean onQueryTextSubmit ( String query ) {
        searchResult.query( query );

        getFragmentManager().beginTransaction()
                .hide( searchResult )
                .show( loading )
                .commit();

        return true;
    }

    @Override
    public boolean onQueryTextChange ( String newText ) {
        return false;
    }

    // ---------------------------------------------------------------------------------------------
    // SearchResultFragment.OnResultListener interface
    // ---------------------------------------------------------------------------------------------

    @Override
    public void onResult ( List< SearchResult > results ) {
        getFragmentManager().beginTransaction()
                .hide( loading )
                .show( searchResult )
                .commit();
    }

}

package papagaio.app.feature.search;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import papagaio.app.R;
import papagaio.app.adapter.ListRecyclerAdapter;
import papagaio.app.util.LaunchIntentClickListener;
import papagaio.app.util.intent.IntentFactory;

public class SearchResultAdapter extends ListRecyclerAdapter< SearchResult, SearchResultAdapter.ViewHolder > {

    private final Context context;

    public SearchResultAdapter ( Context context ) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder ( ViewGroup parent, int viewType ) {
        View view = LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.listitem_search_result, parent, false );

        return new ViewHolder( view );
    }

    @Override
    public void onBindViewHolder ( ViewHolder holder, int position ) {
        SearchResult result = get( position );

        holder.textTitle.setText( result.getTitle() );

        IntentFactory intentFactory = result.getIntentFactory();
        if ( intentFactory != null ) {
            Intent intent = intentFactory.create( context );
            holder.view.setOnClickListener( new LaunchIntentClickListener( intent ) );
        }
    }

    // ---------------------------------------------------------------------------------------------

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public final TextView textTitle;

        public ViewHolder ( View itemView ) {
            super( itemView );
            view = itemView;
            textTitle = ( TextView ) itemView.findViewById( R.id.result_title );
        }
    }

}

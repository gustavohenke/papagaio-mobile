package papagaio.app.feature.hashtag;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import papagaio.app.R;
import papagaio.app.feature.timeline.TimelineActivity;
import papagaio.app.fragment.postList.PostListFactory;

public class HashTagActivity extends TimelineActivity {

    public static final String EXTRA_HASHTAG = "hashtag";
    private String hashtag;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        final Intent intent = getIntent();
        if ( Intent.ACTION_VIEW.equals( intent.getAction() ) ) {
            final Uri data = intent.getData();
            String hashtagPrefix = getString( R.string.path_prefix_hashtag ).replaceAll( "^/?(.+?)/?$", "" );
            String[] prefixParts = hashtagPrefix.split( "/" );
            hashtag = data.getPathSegments().get( prefixParts.length );
        } else {
            hashtag = intent.getStringExtra( EXTRA_HASHTAG );
        }

        setTitle( "#" + hashtag );
        super.onCreate( savedInstanceState );
    }

    @Override
    public PostListFactory getPostListFactory () {
        return new HashTagPostListFactory( hashtag );
    }
}

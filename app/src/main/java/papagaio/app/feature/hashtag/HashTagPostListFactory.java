package papagaio.app.feature.hashtag;

import android.os.Parcel;

import papagaio.app.fragment.postList.PostListFactory;
import papagaio.app.model.Request;
import papagaio.app.model.post.ListHashTagPostRequest;
import papagaio.app.model.post.Post;

public class HashTagPostListFactory implements PostListFactory {

    private final String hashtag;

    public HashTagPostListFactory ( String hashtag ) {
        this.hashtag = hashtag;
    }

    @Override
    public Request< Post.List > createRequest () {
        return new ListHashTagPostRequest( hashtag );
    }

    // ---------------------------------------------------------------------------------------------
    // Parcelable interface
    // ---------------------------------------------------------------------------------------------

    protected HashTagPostListFactory ( Parcel in ) {
        this.hashtag = in.readString();
    }

    @Override
    public int describeContents () {
        return 0;
    }

    @Override
    public void writeToParcel ( Parcel dest, int flags ) {
        dest.writeString( hashtag );
    }

    public static final Creator< HashTagPostListFactory > CREATOR = new Creator< HashTagPostListFactory >() {
        @Override
        public HashTagPostListFactory createFromParcel ( Parcel in ) {
            return new HashTagPostListFactory( in );
        }

        @Override
        public HashTagPostListFactory[] newArray ( int size ) {
            return new HashTagPostListFactory[ size ];
        }
    };
}

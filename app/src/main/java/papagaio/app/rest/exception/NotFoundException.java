package papagaio.app.rest.exception;

public class NotFoundException extends RestException {

    /**
     * Instancia com uma mensagem de erro
     *
     * @param message   a mensagem de erro
     */
    public NotFoundException ( String message ) {
        super( message );
    }
}

package papagaio.app.rest.exception;

public class AuthenticationException extends RestException {

    /**
     * Instancia com uma mensagem de erro
     *
     * @param message   a mensagem de erro
     */
    public AuthenticationException ( String message ) {
        super( message );
    }

}

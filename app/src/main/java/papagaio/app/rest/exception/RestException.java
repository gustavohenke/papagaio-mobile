package papagaio.app.rest.exception;

public class RestException extends Exception {

    /**
     * Instancia sem nenhuma mensagem de erro
     */
    public RestException () {
    }

    /**
     * Instancia com uma mensagem de erro
     *
     * @param message   a mensagem de erro
     */
    public RestException ( String message ) {
        super( message );
    }

    /**
     * Instancia com um Throwable causa
     * @param cause     a causa desta exception
     */
    public RestException ( Throwable cause ) {
        super( cause );
    }
}

package papagaio.app.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Proxy;
import java.util.concurrent.TimeUnit;

import papagaio.app.model.follow.FollowAPI;
import papagaio.app.model.like.LikeAPI;
import papagaio.app.model.post.PostAPI;
import papagaio.app.model.session.SessionAPI;
import papagaio.app.model.user.UserAPI;
import papagaio.app.rest.interceptors.AuthInterceptor;
import papagaio.app.rest.interceptors.LoggerInterceptor;
import papagaio.app.rest.proxy.ApiInvocationProxy;
import papagaio.app.util.Constants;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public final class RestAPI {

    // Constantes
    // ---------------------------------------------------------------------------------------------
    private static RestAPI instance;

    // Propriedades
    // ---------------------------------------------------------------------------------------------
    private final Retrofit api;
    private AuthInterceptor authenticator;

    public final SessionAPI session;
    public final PostAPI post;
    public final LikeAPI like;
    public final UserAPI user;
    public final FollowAPI follow;

    // ---------------------------------------------------------------------------------------------

    private RestAPI () {
        final Gson gson = new GsonBuilder().setDateFormat( "yyyy-MM-dd'T'HH:mm:ss" ).create();

        this.api = new Retrofit.Builder()
                .baseUrl( Constants.API_URL )
                .addConverterFactory( GsonConverterFactory.create( gson ) )
                .build();

        authenticator = new AuthInterceptor();

        this.api.client().setConnectTimeout( 5, TimeUnit.SECONDS );
        this.api.client().interceptors().add( new LoggerInterceptor() );
        this.api.client().interceptors().add( authenticator );

        this.post = this.create( PostAPI.class );
        this.session = this.create( SessionAPI.class );
        like = create( LikeAPI.class );
        user = create( UserAPI.class );
        follow = create( FollowAPI.class );
    }

    @SuppressWarnings( "unchecked" )
    private < T > T create ( Class< T > cls ) {
        return ( T ) Proxy.newProxyInstance(
                cls.getClassLoader(),
                new Class[] { cls },
                new ApiInvocationProxy( this.api.create( cls ) )
        );
    }

    /**
     * <p>Define a autenticação da API RESTful.</p>
     * <p>
     *     O uso de um token <code>null</code> resultará na não utilização de nenhuma autenticação.
     * </p>
     *
     * @param   token   Qual token para usar ao autenticar-se
     */
    public void setAuth ( final String token ) {
        authenticator.setToken( token );
    }

    // ---------------------------------------------------------------------------------------------

    public static RestAPI getInstance () {
        if ( instance == null ) {
            instance = new RestAPI();
        }

        return instance;
    }

}

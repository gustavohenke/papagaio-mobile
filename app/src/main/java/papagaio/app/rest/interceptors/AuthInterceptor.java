package papagaio.app.rest.interceptors;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class AuthInterceptor implements Interceptor {

    private String token;

    public void setToken ( String token ) {
        this.token = token;
    }

    @Override
    public Response intercept ( Chain chain ) throws IOException {
        Request request = chain.request();

        if ( token != null ) {
            request = request.newBuilder()
                    .addHeader( "Authorization", "Bearer " + token )
                    .build();
        }

        return chain.proceed( request );
    }
}

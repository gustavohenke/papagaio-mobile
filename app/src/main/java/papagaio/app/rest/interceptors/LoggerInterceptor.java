package papagaio.app.rest.interceptors;

import android.util.Log;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

public class LoggerInterceptor implements Interceptor {

    private static final String TAG = "RestAPI";

    @Override
    public Response intercept ( Chain chain ) throws IOException {
        long before = System.nanoTime();

        Request request = chain.request();
        Response response = chain.proceed( request );

        long duration = System.nanoTime() - before;
        request = response.request();

        Log.i( TAG, String.format(
                "%s %s - %d - %.1f ms",
                request.method(),
                request.urlString(),
                response.code(),
                duration / 1e6d
        ));

        return response;
    }
}

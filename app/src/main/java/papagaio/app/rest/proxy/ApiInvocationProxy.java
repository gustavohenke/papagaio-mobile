package papagaio.app.rest.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import retrofit.Call;

public class ApiInvocationProxy implements InvocationHandler {
    private final Object impl;

    public ApiInvocationProxy ( Object impl ) {
        this.impl = impl;
    }

    @Override
    public Object invoke ( Object proxy, Method method, Object[] args ) throws Throwable {
        Object result = method.invoke( impl, args );
        if ( !( result instanceof Call ) ) {
            return result;
        }

        return Proxy.newProxyInstance(
                Call.class.getClassLoader(),
                new Class[] { Call.class },
                new CallInvocationProxy( result )
        );
    }
}

package papagaio.app.rest.proxy;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import papagaio.app.rest.exception.AuthenticationException;
import papagaio.app.rest.exception.RestException;
import retrofit.Response;

public class CallInvocationProxy implements InvocationHandler {

    private final static JsonParser jsonParser = new JsonParser();
    private final Object implementation;

    public CallInvocationProxy ( Object implementation ) {
        this.implementation = implementation;
    }

    @Override
    public Object invoke ( Object proxy, Method method, Object[] args ) throws Throwable {
        try {
            Object result = method.invoke( this.implementation, args );
            if ( !( result instanceof Response ) ) {
                return result;
            }

            Response response = ( Response ) result;
            if ( response.isSuccess() ) {
                return response;
            }

            String message = getMessage( response.errorBody() );
            switch ( response.code() ) {
                case 401:
                    throw new AuthenticationException( message );

                default:
                    throw new RestException( message );
            }
        } catch ( InvocationTargetException e ) {
            throw e.getCause();
        }
    }

    private static String getMessage ( ResponseBody body ) throws IOException {
        JsonObject object = jsonParser.parse( body.string() ).getAsJsonObject();
        return String.format(
                "[%s] %s",
                object.get( "err" ).getAsString(),
                object.get( "message" ).getAsString()
        );
    }
}

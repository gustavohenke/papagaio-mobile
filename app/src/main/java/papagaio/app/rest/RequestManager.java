package papagaio.app.rest;

import android.util.Log;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.Collections;
import java.util.List;

import papagaio.app.App;
import papagaio.app.model.Listener;
import papagaio.app.model.Request;
import papagaio.app.service.SpiceService;
import roboguice.util.temp.Ln;

public class RequestManager {

    private static final RequestManager instance = new RequestManager();
    private final SpiceManager spiceManager = new SpiceManager( SpiceService.class );

    public static RequestManager getInstance () {
        return instance;
    }

    private RequestManager () {
        Ln.getConfig().setLoggingLevel( Log.WARN );
        spiceManager.start( App.getContext() );
    }

    public < T > void execute ( Request< T > request, long cacheDuration ) {
        App.bus.post( request );

        RequestListener< T > listener = createRequestListener( request.getListeners() );
        spiceManager.execute( request, request.getCacheKey(), cacheDuration, listener );
    }

    /**
     * Adiciona um listener a um request que está pendente.
     *
     * @param request   O objeto de requisição
     * @param listener  O objeto listener para esta requisição
     * @param <T>       O tipo de retorno usado na requisição
     */
    @SuppressWarnings( "deprecated" )
    public < T > void listenToPending ( Request< T > request, Listener< T > listener ) {
        RequestListener< T > requestListener = createRequestListener( listener );
        spiceManager.addListenerIfPending(
                request.getResultType(),
                request.getCacheKey(),
                requestListener
        );
    }

    /**
     * Remove os dados de cache de uma requisição
     *
     * @param request
     */
    public void removeCache ( Request< ? > request ) {
        spiceManager.removeDataFromCache( request.getResultType(), request.getCacheKey() );
    }

    public < T > void cancel ( Request< T > request ) {
        spiceManager.cancel( request.getResultType(), request.getCacheKey() );
    }

    /**
     * Cria um listener para o RoboSpice a partir de outro listener.
     *
     * @param listener
     * @param <T>
     * @return
     */
    private < T > RequestListener< T > createRequestListener ( Listener< T > listener ) {
        return createRequestListener( Collections.singletonList( listener ) );
    }

    /**
     * Cria um listener para o RoboSpice com uma lista de outros listeners.
     *
     * @param listeners A lista de listeners
     * @param <T>
     * @return
     */
    private < T > RequestListener< T > createRequestListener ( List< Listener< T > > listeners ) {
        RequestListener< T > listener = null;
        if ( !listeners.isEmpty() ) {
            listener = new ListRequestListener<>( listeners );
        }

        return listener;
    }

}
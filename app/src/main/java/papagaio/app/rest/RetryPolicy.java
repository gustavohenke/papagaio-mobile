package papagaio.app.rest;

import com.octo.android.robospice.retry.DefaultRetryPolicy;

public class RetryPolicy extends DefaultRetryPolicy {

    public RetryPolicy () {
        super( 1, DEFAULT_DELAY_BEFORE_RETRY, DEFAULT_BACKOFF_MULT );
    }

}

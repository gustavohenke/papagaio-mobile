package papagaio.app.rest;

import com.octo.android.robospice.exception.NoNetworkException;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.List;

import papagaio.app.model.Listener;

class ListRequestListener< RESULT > implements RequestListener< RESULT > {

    private final List< Listener< RESULT > > listeners;

    public ListRequestListener ( List< Listener< RESULT > > listeners ) {
        this.listeners = listeners;
    }

    @Override
    public final void onRequestFailure ( SpiceException spiceException ) {
        Exception exception = spiceException instanceof NoNetworkException
                ? spiceException
                : ( Exception ) spiceException.getCause();

        if ( exception instanceof UndeclaredThrowableException ) {
            exception = ( Exception ) exception.getCause();
        }

        for ( Listener< RESULT > listener : listeners ) {
            listener.always( null, exception );
            listener.failure( exception );
        }
    }

    @Override
    public final void onRequestSuccess ( RESULT result ) {
        for ( Listener< RESULT > listener : listeners ) {
            listener.always( result, null );
            listener.success( result );
        }
    }
}

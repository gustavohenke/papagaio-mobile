package papagaio.app;

import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import papagaio.app.feature.timeline.TimelineActivity;
import papagaio.app.service.AuthService;
import papagaio.app.util.Constants;
import papagaio.app.util.exceptionHandler.DialogExceptionHandler;

public class MainActivity extends Activity {

    private SharedPreferences prefs;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        final TokenAcquiredCallback callback = new TokenAcquiredCallback();
        prefs = getSharedPreferences( Constants.PREFS_APP, MODE_PRIVATE );
        String username = prefs.getString( Constants.PREFS_APP_USERNAME, "" );
        AccountManagerFuture< Bundle > future = null;

        if ( !username.equals( "" ) ) {
            future = AuthService.getAccountToken( username, this, callback );
        }

        if ( future == null ) {
            AuthService.getAccountToken( this, callback );
        }
    }

    private class TokenAcquiredCallback implements AccountManagerCallback< Bundle > {
        @Override
        public void run ( AccountManagerFuture< Bundle > future ) {
            try {
                final Bundle result = future.getResult();
                final String token = result.getString( AccountManager.KEY_AUTHTOKEN );
                final String accountName = result.getString( AccountManager.KEY_ACCOUNT_NAME );

                if ( token != null ) {
                    AuthService.setAuth( token );

                    Intent intent = new Intent( MainActivity.this, TimelineActivity.class );
                    startActivity( intent );

                    prefs.edit()
                            .putString( Constants.PREFS_APP_TOKEN, token )
                            .putString( Constants.PREFS_APP_USERNAME, accountName )
                            .apply();
                }
            } catch ( Exception e ) {
                new DialogExceptionHandler().handle( MainActivity.this, e );
            }
        }
    }
}

package papagaio.app.fragment.postList;

import android.os.Parcelable;

import papagaio.app.model.Request;
import papagaio.app.model.post.Post;

public interface PostListFactory extends Parcelable {

    Request< Post.List > createRequest();

}

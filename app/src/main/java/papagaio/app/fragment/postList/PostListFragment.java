package papagaio.app.fragment.postList;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.List;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import de.greenrobot.event.ThreadMode;
import papagaio.app.R;
import papagaio.app.adapter.PostListAdapter;
import papagaio.app.model.Listener;
import papagaio.app.model.Request;
import papagaio.app.model.like.DislikePostRequest;
import papagaio.app.model.like.LikePostRequest;
import papagaio.app.model.post.CreatePostRequest;
import papagaio.app.model.post.Post;
import papagaio.app.rest.RequestManager;
import papagaio.app.util.exceptionHandler.SnackbarExceptionHandler;

public class PostListFragment extends Fragment implements PostListAdapter.LikeAction {

    // ---------------------------------------------------------------------------------------------
    // Constantes
    // ---------------------------------------------------------------------------------------------
    private static final String ARG_FACTORY = "factory";

    // ---------------------------------------------------------------------------------------------
    // Propriedades
    // ---------------------------------------------------------------------------------------------
    private PostListAdapter adapter;
    private OnReadyListener listener;
    private View loader;
    private PostListFactory factory;
    private RecyclerView list;

    // ---------------------------------------------------------------------------------------------
    // Factory method
    // ---------------------------------------------------------------------------------------------
    public static PostListFragment newInstance ( PostListFactory factory ) {
        final PostListFragment fragment = new PostListFragment();

        final Bundle args = new Bundle();
        args.putParcelable( ARG_FACTORY, factory );
        fragment.setArguments( args );

        return fragment;
    }

    // ---------------------------------------------------------------------------------------------
    // Fragment interface
    // ---------------------------------------------------------------------------------------------

    public PostListFragment () {}

    @Override
    public void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        factory = getArguments().getParcelable( ARG_FACTORY );
        adapter = new PostListAdapter( getActivity() );
        adapter.setOnLikeListener( this );
    }

    @Override
    public View onCreateView ( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        return inflater.inflate( R.layout.list, container, false );
    }

    @Override
    public void onActivityCreated ( Bundle savedInstanceState ) {
        super.onActivityCreated( savedInstanceState );

        loader = getView().findViewById( R.id.list_progressbar );

        list = ( RecyclerView ) getView().findViewById( android.R.id.list );
        list.setLayoutManager( new LinearLayoutManager( getActivity() ) );
        list.setAdapter( adapter );

        /*LayoutInflater inflater = LayoutInflater.from( getActivity() );

        View footer = inflater.inflate( R.layout.list_footer, null, false );
        list.addFooterView( footer );*/
    }

    @Override
    public void onStart () {
        super.onStart();
        fetch( false );
    }

    @Override
    public void onAttach ( Activity activity ) {
        super.onAttach( activity );
        EventBus.getDefault().register( this );

        try {
            listener = ( OnReadyListener ) activity;
        } catch ( ClassCastException e ) {
            throw new ClassCastException( activity.toString() +
                    " deve implementar OnReadyListener" );
        }
    }

    @Override
    public void onDetach () {
        super.onDetach();
        EventBus.getDefault().unregister( this );
        listener = null;
    }

    @Override
    @SuppressWarnings( "unchecked" )
    public void onLike ( final Post post, final View view ) {
        toggleLike( post );

        Request request = post.isLiked()
                ? new LikePostRequest( post.getId() )
                : new DislikePostRequest( post.getId() );

        Request opposite = post.isLiked()
                ? new DislikePostRequest( post.getId() )
                : new LikePostRequest( post.getId() );

        final Context context = getActivity();
        RequestManager.getInstance().cancel( opposite );

        request.execute( new Listener() {
            @Override
            public void success ( Object o ) {

            }

            @Override
            public void failure ( Exception exception ) {
                toggleLike( post );
                adapter.toggleLikeStatus( post, view );

                new SnackbarExceptionHandler(
                        getActivity().findViewById( android.R.id.content ),
                        Snackbar.LENGTH_LONG
                ).handle( context, exception );
            }
        });
    }

    private void toggleLike ( Post post ) {
        boolean liked = post.isLiked();
        post.setLiked( !liked );
        post.setLikesCount( post.getLikesCount() + ( liked ? -1 : 1 ) );
    }

    @Subscribe( threadMode = ThreadMode.MainThread )
    public void onEvent ( CreatePostRequest createPost ) {
        toggleLoader( true );
        createPost.listen( new ClearAfterNewPostListener() );
    }

    private void fetch ( boolean clearCache ) {
        if ( !adapter.isEmpty() ) {
            toggleLoader( true );
        }

        Request< Post.List > request = factory.createRequest();
        request.execute( new ListPostListener() );
    }

    private void toggleLoader ( boolean toggle ) {
        loader.setVisibility( toggle ? View.VISIBLE : View.GONE );
    }

    // ---------------------------------------------------------------------------------------------

    class ClearAfterNewPostListener extends Listener< Post > {

        @Override
        public void success ( Post post ) {
            fetch( true );
        }

        @Override
        public void failure ( Exception exception ) {

        }

        @Override
        public void always ( Post post, Exception exception ) {
            super.always( post, exception );
            toggleLoader( false );
        }
    }

    // ---------------------------------------------------------------------------------------------

    class ListPostListener extends Listener< Post.List > {

        @Override
        public void failure ( Exception exception ) {
            listener.onReady( null, exception );
        }

        @Override
        public void success ( Post.List posts ) {
            list.smoothScrollToPosition( 0 );

            adapter.clear();
            adapter.addAll( posts );
            adapter.notifyDataSetChanged();

            if ( listener != null ) {
                listener.onReady( posts, null );
            }
        }

        @Override
        public void always ( Post.List posts, Exception exception ) {
            super.always( posts, exception );
            toggleLoader( false );
        }
    }

    // ---------------------------------------------------------------------------------------------

    public interface OnReadyListener {

        void onReady ( List< Post > posts, Exception exception );

    }
}

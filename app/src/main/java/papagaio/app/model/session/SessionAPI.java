package papagaio.app.model.session;

import papagaio.app.rest.exception.AuthenticationException;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.POST;

public interface SessionAPI {

    @POST("session")
    Call<Session> create(@Body Credentials credentials) throws AuthenticationException;

}

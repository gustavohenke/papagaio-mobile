package papagaio.app.model.session;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;
import papagaio.app.rest.exception.AuthenticationException;
import retrofit.Response;

public class LoginRequest extends Request< String > {

    private final Credentials credentials;

    public LoginRequest ( Credentials credentials ) {
        super( String.class );
        this.credentials = credentials;
    }

    @Override
    public Object getCacheKey () {
        return "login";
    }

    @Override
    public String loadDataFromNetwork () throws Exception {
        RestAPI rest = RestAPI.getInstance();

        try {
            SessionAPI sessionAPI = rest.session;
            Response< Session > session = sessionAPI.create( credentials ).execute();
            if ( session.isSuccess() ) {
                String token = session.body().getId();
                rest.setAuth( token );

                return token;
            }
        } catch ( AuthenticationException e ) {
            e.printStackTrace();
        }

        rest.setAuth( null );
        return null;
    }
}

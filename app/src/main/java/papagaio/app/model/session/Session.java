package papagaio.app.model.session;

import java.util.Date;

import papagaio.app.model.user.User;

public class Session {
    private String id;
    private User user;
    private Date createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser () {
        return user;
    }

    public void setUser ( User user ) {
        this.user = user;
    }

    public Date getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}

package papagaio.app.model.user;

import android.text.TextUtils;

import com.octo.android.robospice.persistence.DurationInMillis;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;

public class ListUserRequest extends Request< User.List > {

    private final String search;

    public ListUserRequest () {
        super( User.List.class );
        search = null;
    }

    public ListUserRequest ( String search ) {
        super( User.List.class );
        this.search = search;
    }

    @Override
    public Object getCacheKey () {
        return "user:list" + ( TextUtils.isEmpty( search ) ? "" : ":" + search );
    }

    @Override
    public User.List loadDataFromNetwork () throws Exception {
        return RestAPI.getInstance().user.findAll( search ).execute().body();
    }
}

package papagaio.app.model.user;

import java.util.ArrayList;
import java.util.Date;

import papagaio.app.model.misc.Image;

public class User {

    private int id;
    private String username;
    private String password;
    private String email;
    private Image photo;
    private Image cover;
    private Date createdAt;
    private int followersCount;
    private int followingCount;

    public int getId () {
        return id;
    }

    public void setId ( int id ) {
        this.id = id;
    }

    public String getUsername () {
        return username;
    }

    public void setUsername ( String username ) {
        this.username = username;
    }

    public String getPassword () {
        return password;
    }

    public void setPassword ( String password ) {
        this.password = password;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail ( String email ) {
        this.email = email;
    }

    public Image getPhoto () {
        return photo;
    }

    public void setPhoto ( Image photo ) {
        this.photo = photo;
    }

    public Image getCover () {
        return cover;
    }

    public void setCover ( Image cover ) {
        this.cover = cover;
    }

    public Date getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt ( Date createdAt ) {
        this.createdAt = createdAt;
    }

    public int getFollowersCount () {
        return followersCount;
    }

    public void setFollowersCount ( int followersCount ) {
        this.followersCount = followersCount;
    }

    public int getFollowingCount () {
        return followingCount;
    }

    public void setFollowingCount ( int followingCount ) {
        this.followingCount = followingCount;
    }

    /**
     * <p>Obtem o nome do usuário, no formato padrão do Papagaio, com o prefixo "@".</p>
     * <p>
     *     Por exemplo, se o nome do usuário é "papagaio", o nome de usuário padronizado será
     *     "@papagaio"
     * </p>
     *
     * @return  O nome de usuário no formato padrão
     */
    public String getStandardUsername () {
        return "@" + getUsername();
    }

    // ---------------------------------------------------------------------------------------------

    public static class List extends ArrayList< User > {}
}

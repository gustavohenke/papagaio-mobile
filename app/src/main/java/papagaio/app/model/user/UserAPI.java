package papagaio.app.model.user;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface UserAPI {

    @POST( "user" )
    Call< User > create ( @Body User user );

    @GET( "user/{userId}" )
    Call< User > get ( @Path( "userId" ) int userId );

    @GET( "user/{username}" )
    Call< User > get ( @Path( "username" ) String username );

    @GET( "user" )
    Call< User.List > findAll ( @Query( "search" ) String searchTerm );
}

package papagaio.app.model.user;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;
import retrofit.Call;

public class GetUserRequest extends Request< User > {

    private final int userId;
    private final String username;

    public GetUserRequest ( int userId ) {
        super( User.class );
        this.userId = userId;
        this.username = null;
    }

    public GetUserRequest ( String username ) {
        super( User.class );
        this.userId = 0;
        this.username = username;
    }

    @Override
    public Object getCacheKey () {
        return "user." + ( username != null ? username : userId );
    }

    @Override
    public User loadDataFromNetwork () throws Exception {
        UserAPI userAPI = RestAPI.getInstance().user;
        Call< User > call = username != null ? userAPI.get( username ) : userAPI.get( userId );
        return call.execute().body();
    }
}

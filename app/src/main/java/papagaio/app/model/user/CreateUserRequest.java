package papagaio.app.model.user;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;

public class CreateUserRequest extends Request< User > {

    private final User user;

    public CreateUserRequest ( User user ) {
        super( User.class );
        this.user = user;
    }

    @Override
    public Object getCacheKey () {
        return "user:create";
    }

    @Override
    public User loadDataFromNetwork () throws Exception {
        return RestAPI.getInstance().user.create( user ).execute().body();
    }
}

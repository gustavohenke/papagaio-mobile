package papagaio.app.model;

public abstract class Listener< RESULT > {

    public void always ( RESULT result, Exception exception ) {}
    public abstract void success ( RESULT result );
    public abstract void failure ( Exception exception );

}
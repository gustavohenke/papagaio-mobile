package papagaio.app.model;

import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.request.SpiceRequest;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import papagaio.app.rest.RequestManager;
import papagaio.app.rest.RetryPolicy;

public abstract class Request< RESULT > extends SpiceRequest< RESULT > {

    private final List< Listener< RESULT > > listeners = new LinkedList<>();

    public Request ( Class< RESULT > clazz ) {
        super( clazz );
        setRetryPolicy( new RetryPolicy() );
    }

    public abstract Object getCacheKey ();

    /**
     * <p>Define o tempo de expiração de cache padrão.</p>
     * <p>
     *     Usado quando o parâmetro <code>cacheDuration</code> não é usado em
     *     {@link #execute(long, Listener)}.
     * </p>
     *
     * @return
     */
    public long getDefaultCacheDuration () {
        return DurationInMillis.ALWAYS_EXPIRED;
    }

    public final RESULT executeSync () throws Exception {
        final SyncResult syncResult = new SyncResult();
        final CountDownLatch doneSignal = new CountDownLatch( 1 );

        execute( new Listener< RESULT >() {
            @Override
            public void always ( RESULT result, Exception exception ) {
                syncResult.exception = exception;
                syncResult.result = result;
                doneSignal.countDown();
            }

            @Override
            public void success ( RESULT result ) {

            }

            @Override
            public void failure ( Exception exception ) {

            }
        });

        doneSignal.await();
        if ( syncResult.exception != null ) {
            throw syncResult.exception;
        }

        return syncResult.result;
    }

    public void listen ( Listener< RESULT > listener ) {
        listeners.add( listener );
    }

    public final void execute () {
        RequestManager.getInstance().execute( this, getDefaultCacheDuration() );
    }

    public final void execute ( Listener< RESULT > listener ) {
        execute( getDefaultCacheDuration(), listener );
    }

    public final void execute ( long cacheDuration, Listener< RESULT > listener ) {
        listen( listener );
        RequestManager.getInstance().execute( this, cacheDuration );
    }

    public List< Listener< RESULT > > getListeners () {
        return listeners;
    }

    private class SyncResult {
        public RESULT result;
        public Exception exception;
    }
}

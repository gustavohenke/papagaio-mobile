package papagaio.app.model.follow;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;

public class UnfollowRequest extends Request< Void > {
    private String username;

    public UnfollowRequest ( String username ) {
        super( Void.class );
        this.username = username;
    }

    @Override
    public Object getCacheKey () {
        return "unfollow:" + username;
    }

    @Override
    public Void loadDataFromNetwork () throws Exception {
        return RestAPI.getInstance().follow.unfollow( username ).execute().body();
    }
}

package papagaio.app.model.follow;

import com.octo.android.robospice.persistence.DurationInMillis;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;
import retrofit.Call;

public class IsFollowingRequest extends Request< Void > {

    private final int userId;
    private final String username;

    public IsFollowingRequest ( String username ) {
        super( Void.class );
        this.userId = 0;
        this.username = username;
    }

    public IsFollowingRequest ( int userId ) {
        super( Void.class );
        this.userId = userId;
        this.username = null;
    }

    @Override
    public Object getCacheKey () {
        return "isfollowing:" + ( username == null ? userId : username );
    }

    @Override
    public long getDefaultCacheDuration () {
        return DurationInMillis.ALWAYS_EXPIRED;
    }

    @Override
    public Void loadDataFromNetwork () throws Exception {
        FollowAPI followAPI = RestAPI.getInstance().follow;
        Call< Void > call = username == null ?
                followAPI.isFollowing( userId ) :
                followAPI.isFollowing( username );

        return call.execute().body();
    }
}

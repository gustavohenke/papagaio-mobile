package papagaio.app.model.follow;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;

public class FollowRequest extends Request< Void > {
    private String username;

    public FollowRequest ( String username ) {
        super( Void.class );
        this.username = username;
    }

    @Override
    public Object getCacheKey () {
        return "follow:" + username;
    }

    @Override
    public Void loadDataFromNetwork () throws Exception {
        FollowData data = new FollowData( username );
        return RestAPI.getInstance().follow.follow( data ).execute().body();
    }
}

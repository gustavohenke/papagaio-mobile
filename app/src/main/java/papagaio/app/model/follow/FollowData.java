package papagaio.app.model.follow;

public class FollowData {

    private final String user;

    public FollowData ( String user ) {
        this.user = user;
    }

    public String getUser () {
        return user;
    }
}

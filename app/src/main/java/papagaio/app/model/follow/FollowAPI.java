package papagaio.app.model.follow;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface FollowAPI {

    @GET( "user/me/following/{username}" )
    Call< Void > isFollowing ( @Path( "username" ) String username );

    @GET( "user/me/following/{userId}" )
    Call< Void > isFollowing ( @Path( "userId" ) int userId );

    @POST( "user/me/following" )
    Call< Void > follow ( @Body FollowData data );

    @DELETE( "user/me/following/{username}" )
    Call< Void > unfollow ( @Path( "username" ) String username );

}

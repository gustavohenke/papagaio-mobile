package papagaio.app.model.like;

import retrofit.Call;
import retrofit.http.DELETE;
import retrofit.http.PUT;
import retrofit.http.Path;

public interface LikeAPI {

    @PUT( "post/{postId}/like" )
    Call< Void > create ( @Path( "postId" ) long postId );

    @DELETE( "post/{postId}/like" )
    Call< Void > remove ( @Path( "postId" ) long postId );

}

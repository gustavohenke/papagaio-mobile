package papagaio.app.model.like;

import java.util.Date;

import papagaio.app.model.post.Post;
import papagaio.app.model.user.User;

public class Like {

    private long id;
    private Post post;
    private User user;
    private Date createdAt;

    public Date getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt ( Date createdAt ) {
        this.createdAt = createdAt;
    }

    public long getId () {
        return id;
    }

    public void setId ( long id ) {
        this.id = id;
    }

    public Post getPost () {
        return post;
    }

    public void setPost ( Post post ) {
        this.post = post;
    }

    public User getUser () {
        return user;
    }

    public void setUser ( User user ) {
        this.user = user;
    }
}

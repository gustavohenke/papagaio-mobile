package papagaio.app.model.like;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;

public class DislikePostRequest extends Request< Void > {
    private long postId;

    public DislikePostRequest ( long postId ) {
        super( Void.class );
        this.postId = postId;
    }

    @Override
    public Object getCacheKey () {
        return "like.remove";
    }

    @Override
    public Void loadDataFromNetwork () throws Exception {
        return RestAPI.getInstance().like.remove( postId ).execute().body();
    }
}

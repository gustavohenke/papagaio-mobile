package papagaio.app.model.like;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;

public class LikePostRequest extends Request< Void > {

    private final long postID;

    public LikePostRequest ( long postID ) {
        super( Void.class );
        this.postID = postID;
    }

    @Override
    public Object getCacheKey () {
        return "like.create";
    }

    @Override
    public Void loadDataFromNetwork () throws Exception {
        return RestAPI.getInstance().like.create( postID ).execute().body();
    }
}

package papagaio.app.model.location;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import papagaio.app.App;
import papagaio.app.model.Request;

public class GetLocationRequest extends Request< Location > {

    private static final String LOG_TAG = GetLocationRequest.class.getSimpleName();
    private static final int LISTENER_ATTEMPTS = 5;
    private static final long LISTENER_TIMEOUT = 10;
    private android.location.Location providerLocation;

    public GetLocationRequest () {
        super( Location.class );
    }

    @Override
    public Object getCacheKey () {
        return "location";
    }

    @Override
    public Location loadDataFromNetwork () throws Exception {
        Context context = App.getContext();
        LocationManager locMgr = ( LocationManager ) context.getSystemService( Context.LOCATION_SERVICE );

        int coarseLocation = ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
        );
        int fineLocation = ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
        );

        boolean permitted =
                coarseLocation == PackageManager.PERMISSION_GRANTED ||
                fineLocation == PackageManager.PERMISSION_GRANTED;

        if ( !permitted ) {
            throw new LocationNotAllowedException();
        }

        final CountDownLatch latch = new CountDownLatch( LISTENER_ATTEMPTS );

        LocationListener listener = new LocationListener() {

            @Override
            public void onLocationChanged ( android.location.Location location ) {
                providerLocation = location;
                Log.d( LOG_TAG, String.format( "provider got location: %s, %s",
                        location.getLatitude(),
                        location.getLongitude()
                ));

                while ( latch.getCount() > 0 ) {
                    latch.countDown();
                }
            }

            @Override
            public void onStatusChanged ( String provider, int status, Bundle extras ) {
                Log.d( LOG_TAG, "provider status: " + provider + " - " + status );
                if ( status != LocationProvider.AVAILABLE ) {
                    latch.countDown();
                }
            }

            @Override
            public void onProviderEnabled ( String provider ) {
                Log.d( LOG_TAG, "provider enabled: " + provider );
            }

            @Override
            public void onProviderDisabled ( String provider ) {
                Log.d( LOG_TAG, "provider disabled: " + provider );
                latch.countDown();
            }
        };

        locMgr.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                1,
                0,
                listener,
                Looper.getMainLooper()
        );
        final boolean latchResult = latch.await( LISTENER_TIMEOUT, TimeUnit.SECONDS );

        // Finalizado o CountDownLatch, podemos finalizar também o listener de posicionamento.
        // Evita de torrar bateria.
        locMgr.removeUpdates( listener );

        if ( !latchResult || providerLocation == null ) {
            throw new LocationNotAvailableException();
        }

        Location location = new Location();
        location.setLatitude( ( float ) providerLocation.getLatitude() );
        location.setLongitude( ( float ) providerLocation.getLongitude() );

        String name = new GeocodeRequest( location ).executeSync();
        location.setName( name );

        return location;
    }
}

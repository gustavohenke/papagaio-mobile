package papagaio.app.model.location;

import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.octo.android.robospice.persistence.DurationInMillis;

import java.util.Collections;
import java.util.List;

import papagaio.app.App;
import papagaio.app.model.Request;

public class GeocodeRequest extends Request< String > {

    private static final int GEOCODER_RESULTS = 15;
    private final Location location;

    public GeocodeRequest ( Location location ) {
        super( String.class );
        this.location = location;
    }

    @Override
    public long getDefaultCacheDuration () {
        return DurationInMillis.ALWAYS_RETURNED;
    }

    @Override
    public Object getCacheKey () {
        return "geocode:" + location.toString();
    }

    @Override
    public String loadDataFromNetwork () throws Exception {
        if ( !Geocoder.isPresent() ) {
            return null;
        }

        Geocoder geocoder = new Geocoder( App.getContext() );
        List< Address > addresses = geocoder.getFromLocation(
                location.getLatitude(),
                location.getLongitude(),
                GEOCODER_RESULTS
        );

        Collections.sort( addresses, new AddressComparator() );
        Address address = addresses.get( addresses.size() - 1 );

        return new AddressLine( address ).toString();
    }
}

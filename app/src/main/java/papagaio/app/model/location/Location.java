package papagaio.app.model.location;

import android.os.Parcel;
import android.os.Parcelable;

public class Location implements Parcelable {

    private float latitude;
    private float longitude;
    private String name;

    public Location () {}

    public float getLatitude () {
        return latitude;
    }

    public void setLatitude ( float latitude ) {
        this.latitude = latitude;
    }

    public float getLongitude () {
        return longitude;
    }

    public void setLongitude ( float longitude ) {
        this.longitude = longitude;
    }

    public String getName () {
        return name;
    }

    public void setName ( String name ) {
        this.name = name;
    }

    @Override
    public String toString () {
        if ( name != null ) {
            return name;
        }

        return latitude + ", " + longitude;
    }

    // ---------------------------------------------------------------------------------------------

    protected Location ( Parcel in ) {
        latitude = in.readFloat();
        longitude = in.readFloat();
    }

    @Override
    public void writeToParcel ( Parcel dest, int flags ) {
        dest.writeFloat( latitude );
        dest.writeFloat( longitude );
    }

    @Override
    public int describeContents () {
        return 0;
    }

    public static final Creator< Location > CREATOR = new Creator< Location >() {
        @Override
        public Location createFromParcel ( Parcel in ) {
            return new Location( in );
        }

        @Override
        public Location[] newArray ( int size ) {
            return new Location[ size ];
        }
    };
}

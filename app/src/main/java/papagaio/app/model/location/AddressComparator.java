package papagaio.app.model.location;

import android.location.Address;

import java.util.Comparator;

public class AddressComparator implements Comparator< Address > {

    private int calculateScore ( Address address ) {
        int score = 0;
        if ( address.getCountryName() != null ) {
            score += 1;
        }

        if ( address.getAdminArea() != null ) {
            score += 2;
        }

        if ( address.getLocality() != null ) {
            score += 4;

            if ( address.getFeatureName() != null ) {
                score += 8;
            }

            if ( address.getPostalCode() != null ) {
                score += 16;
            }
        }

        return score;
    }

    @Override
    public int compare ( Address lhs, Address rhs ) {
        int scoreLhs = calculateScore( lhs );
        int scoreRhs = calculateScore( rhs );

        return scoreLhs < scoreRhs ? -1 : ( scoreRhs == scoreLhs ? 0 : 1 );
    }
}

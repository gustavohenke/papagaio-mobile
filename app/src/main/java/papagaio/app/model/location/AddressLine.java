package papagaio.app.model.location;

import android.location.Address;

public class AddressLine {

    private final Address address;

    public AddressLine ( Address address ) {
        this.address = address;
    }

    @Override
    public String toString () {
        String str = "";
        for ( int i = 0; i < address.getMaxAddressLineIndex(); i++ ) {
            if ( i > 0 ) {
                str += ", ";
            }

            str += address.getAddressLine( i );
        }

        return str;
    }
}

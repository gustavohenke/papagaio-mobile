package papagaio.app.model.misc;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Size;

import java.util.ArrayList;

public class Image implements Parcelable {

    private String url;

    public String getUrl () {
        return url;
    }

    public void setUrl ( String url ) {
        this.url = url;
    }

    public String getUrl ( Size size ) {
        return getUrl() + "/" + size;
    }

    // ---------------------------------------------------------------------------------------------

    protected Image ( Parcel in ) {
        url = in.readString();
    }

    public static final Creator< Image > CREATOR = new Creator< Image >() {
        @Override
        public Image createFromParcel ( Parcel in ) {
            return new Image( in );
        }

        @Override
        public Image[] newArray ( int size ) {
            return new Image[ size ];
        }
    };

    @Override
    public int describeContents () {
        return 0;
    }

    @Override
    public void writeToParcel ( Parcel dest, int flags ) {
        dest.writeString( url );
    }

    // ---------------------------------------------------------------------------------------------

    public static class List extends ArrayList< Image > {}
}

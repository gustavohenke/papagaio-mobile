package papagaio.app.model.effect;

import android.media.effect.Effect;
import android.media.effect.EffectContext;

public interface PhotoEffect {

    String getLabel();
    Effect createEffect( EffectContext effectContext );

}

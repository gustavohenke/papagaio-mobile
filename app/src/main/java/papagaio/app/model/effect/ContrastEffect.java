package papagaio.app.model.effect;

import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.media.effect.EffectFactory;

import papagaio.app.App;
import papagaio.app.R;

public class ContrastEffect implements PhotoEffect {
    @Override
    public String getLabel () {
        return App.getContext().getString( R.string.photoeditor_effect_contrast );
    }

    @Override
    public Effect createEffect ( EffectContext effectContext ) {
        Effect contrast = effectContext.getFactory().createEffect( EffectFactory.EFFECT_CONTRAST );
        contrast.setParameter( "contrast", 2f );
        return contrast;
    }
}

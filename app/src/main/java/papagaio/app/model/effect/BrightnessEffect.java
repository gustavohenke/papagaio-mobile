package papagaio.app.model.effect;

import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.media.effect.EffectFactory;

import papagaio.app.App;
import papagaio.app.R;

public class BrightnessEffect implements PhotoEffect {
    @Override
    public String getLabel () {
        return App.getContext().getString( R.string.photoeditor_effect_brightness );
    }

    @Override
    public Effect createEffect ( EffectContext effectContext ) {
        Effect brightness = effectContext.getFactory().createEffect( EffectFactory.EFFECT_BRIGHTNESS );
        brightness.setParameter( "brightness", 2f );
        return brightness;
    }
}

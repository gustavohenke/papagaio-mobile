package papagaio.app.model.effect;

import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.media.effect.EffectFactory;

import papagaio.app.App;
import papagaio.app.R;

public class VignetteEffect implements PhotoEffect {
    @Override
    public String getLabel () {
        return App.getContext().getString( R.string.photoeditor_effect_vignette );
    }

    @Override
    public Effect createEffect ( EffectContext effectContext ) {
        Effect vignette = effectContext.getFactory().createEffect( EffectFactory.EFFECT_VIGNETTE );
        vignette.setParameter( "scale", 1f );
        return vignette;
    }
}

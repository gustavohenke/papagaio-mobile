package papagaio.app.model.effect;

import android.media.effect.Effect;
import android.media.effect.EffectContext;

import papagaio.app.App;
import papagaio.app.R;

public class NoEffect implements PhotoEffect {
    @Override
    public String getLabel () {
        return App.getContext().getString( R.string.photoeditor_effect_none );
    }

    @Override
    public Effect createEffect ( EffectContext effectContext ) {
        return null;
    }
}

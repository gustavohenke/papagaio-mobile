package papagaio.app.model.effect;

import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.media.effect.EffectFactory;

import papagaio.app.App;
import papagaio.app.R;

public class GrayscaleEffect implements PhotoEffect {
    @Override
    public String getLabel () {
        return App.getContext().getString( R.string.photoeditor_effect_grayscale );
    }

    @Override
    public Effect createEffect ( EffectContext effectContext ) {
        return effectContext.getFactory().createEffect( EffectFactory.EFFECT_GRAYSCALE );
    }
}

package papagaio.app.model.effect;

import android.graphics.Bitmap;
import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.opengl.GLES20;
import android.opengl.GLException;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class EffectsRenderer implements GLSurfaceView.Renderer {

    private final Bitmap photo;
    private final int photoWidth;
    private final int photoHeight;
    private EffectContext effectContext;
    private Square square;
    private int[] textures = new int[ 2 ];
    private PhotoEffect photoEffect;
    private OnBitmapGeneratedListener onBitmapGeneratedListener;

    public EffectsRenderer ( Bitmap photo ) {
        this.photo = photo;
        photoWidth = photo.getWidth();
        photoHeight = photo.getHeight();
    }

    // ---------------------------------------------------------------------------------------------
    // GLSurfaceView.Renderer interface
    // ---------------------------------------------------------------------------------------------

    @Override
    public void onSurfaceCreated ( GL10 gl, EGLConfig config ) {
        effectContext = EffectContext.createWithCurrentGlContext();
    }

    @Override
    public void onSurfaceChanged ( GL10 gl, int width, int height ) {
        GLES20.glViewport( 0, 0, width, height );
        GLES20.glClearColor( 0, 0, 0, 1 );
        generateSquare();
    }

    @Override
    public void onDrawFrame ( GL10 gl ) {
        Effect effect = null;
        if ( photoEffect != null ) {
            effect = photoEffect.createEffect( effectContext );
        }

        if ( effect != null ) {
            effect.apply( textures[ 0 ], photoWidth, photoHeight, textures[ 1 ] );
            square.draw( textures[ 1 ] );
            effect.release();
        } else {
            square.draw( textures[ 0 ] );
        }

        if ( onBitmapGeneratedListener != null ) {
            onBitmapGeneratedListener.onBitmapGenerated( generateBitmap( gl ) );
        }
    }

    // ---------------------------------------------------------------------------------------------
    // Getters & Setters
    // ---------------------------------------------------------------------------------------------

    public EffectContext getEffectContext () {
        return effectContext;
    }

    public PhotoEffect getEffect () {
        return photoEffect;
    }

    public void setEffect ( PhotoEffect effect ) {
        this.photoEffect = effect;
    }

    public OnBitmapGeneratedListener getOnBitmapGeneratedListener () {
        return onBitmapGeneratedListener;
    }

    public void setOnBitmapGeneratedListener ( OnBitmapGeneratedListener onBitmapGeneratedListener ) {
        this.onBitmapGeneratedListener = onBitmapGeneratedListener;
    }

    // ---------------------------------------------------------------------------------------------

    private Bitmap generateBitmap ( GL10 gl ) {
        int[] bitmapBuffer = new int[ photoWidth * photoHeight ];
        int[] bitmapSource = new int[ photoWidth * photoHeight ];
        IntBuffer intBuffer = IntBuffer.wrap( bitmapBuffer );
        intBuffer.position( 0 );

        try {
            gl.glReadPixels( 0, 0, photoWidth, photoHeight, GL10.GL_RGBA, GL10.GL_UNSIGNED_BYTE, intBuffer );

            int offset1, offset2;
            for ( int i = 0; i < photoHeight; i++ ) {
                offset1 = i * photoWidth;
                offset2 = ( photoHeight - i - 1 ) * photoWidth;

                for ( int j = 0; j < photoWidth; j++ ) {
                    int texturePixel = bitmapBuffer[ offset1 + j ];
                    int blue = ( texturePixel >> 16 ) & 0xff;
                    int red = ( texturePixel << 16 ) & 0x00ff0000;
                    int pixel = ( texturePixel & 0xff00ff00 ) | red | blue;
                    bitmapSource[ offset2 + j ] = pixel;
                }
            }
        } catch ( GLException e ) {
            return null;
        }

        return Bitmap.createBitmap( bitmapSource, photoWidth, photoHeight, Bitmap.Config.ARGB_8888 );
    }

    private void generateSquare () {
        GLES20.glGenTextures( 2, textures, 0 );
        GLES20.glBindTexture( GLES20.GL_TEXTURE_2D, textures[ 0 ] );

        GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR );
        GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR );
        GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE );
        GLES20.glTexParameteri( GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE );

        GLUtils.texImage2D( GLES20.GL_TEXTURE_2D, 0, photo, 0 );
        square = new Square();
    }

    // ---------------------------------------------------------------------------------------------

    public interface OnBitmapGeneratedListener {
        void onBitmapGenerated ( Bitmap bitmap );
    }

    // ---------------------------------------------------------------------------------------------

    private static class Square {

        private static final String CODE_VERTEX_SHADER =
                "attribute vec4 aPosition;" +
                        "attribute vec2 aTexPosition;" +
                        "varying vec2 vTexPosition;" +
                        "void main() {" +
                        "  gl_Position = aPosition;" +
                        "  vTexPosition = aTexPosition;" +
                        "}";

        private static final String CODE_FRAGMENT_SHADER =
                "precision mediump float;" +
                        "uniform sampler2D uTexture;" +
                        "varying vec2 vTexPosition;" +
                        "void main() {" +
                        "  gl_FragColor = texture2D(uTexture, vTexPosition);" +
                        "}";

        private float vertices[] = {
                -1, -1,
                1, -1,
                -1, 1,
                1, 1,
        };

        private float textureVertices[] = {
                0, 1,
                1, 1,
                0, 0,
                1, 0
        };

        private FloatBuffer verticesBuffer;
        private FloatBuffer textureBuffer;

        private int program;

        public Square () {
            initializeBuffers();
            initializeProgram();
        }

        private void initializeBuffers () {
            ByteBuffer buff = ByteBuffer.allocateDirect( vertices.length * 4 );
            buff.order( ByteOrder.nativeOrder() );
            verticesBuffer = buff.asFloatBuffer();
            verticesBuffer.put( vertices );
            verticesBuffer.position( 0 );

            buff = ByteBuffer.allocateDirect( textureVertices.length * 4 );
            buff.order( ByteOrder.nativeOrder() );
            textureBuffer = buff.asFloatBuffer();
            textureBuffer.put( textureVertices );
            textureBuffer.position( 0 );
        }

        private void initializeProgram () {
            int vertexShader = GLES20.glCreateShader( GLES20.GL_VERTEX_SHADER );
            GLES20.glShaderSource( vertexShader, CODE_VERTEX_SHADER );
            GLES20.glCompileShader( vertexShader );

            int fragmentShader = GLES20.glCreateShader( GLES20.GL_FRAGMENT_SHADER );
            GLES20.glShaderSource( fragmentShader, CODE_FRAGMENT_SHADER );
            GLES20.glCompileShader( fragmentShader );

            program = GLES20.glCreateProgram();
            GLES20.glAttachShader( program, vertexShader );
            GLES20.glAttachShader( program, fragmentShader );

            GLES20.glLinkProgram( program );
        }

        public void draw ( int texture ) {
            GLES20.glBindFramebuffer( GLES20.GL_FRAMEBUFFER, 0 );
            GLES20.glUseProgram( program );
            GLES20.glDisable( GLES20.GL_BLEND );

            int positionHandle = GLES20.glGetAttribLocation( program, "aPosition" );
            int textureHandle = GLES20.glGetUniformLocation( program, "uTexture" );
            int texturePositionHandle = GLES20.glGetAttribLocation( program, "aTexPosition" );

            GLES20.glVertexAttribPointer( texturePositionHandle, 2, GLES20.GL_FLOAT, false, 0, textureBuffer );
            GLES20.glEnableVertexAttribArray( texturePositionHandle );

            GLES20.glActiveTexture( GLES20.GL_TEXTURE0 );
            GLES20.glBindTexture( GLES20.GL_TEXTURE_2D, texture );
            GLES20.glUniform1i( textureHandle, 0 );

            GLES20.glVertexAttribPointer( positionHandle, 2, GLES20.GL_FLOAT, false, 0, verticesBuffer );
            GLES20.glEnableVertexAttribArray( positionHandle );

            GLES20.glClear( GLES20.GL_COLOR_BUFFER_BIT );
            GLES20.glDrawArrays( GLES20.GL_TRIANGLE_STRIP, 0, 4 );
        }

    }

}

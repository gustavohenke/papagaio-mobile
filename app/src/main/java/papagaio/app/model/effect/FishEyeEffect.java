package papagaio.app.model.effect;

import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.media.effect.EffectFactory;

import papagaio.app.App;
import papagaio.app.R;

public class FishEyeEffect implements PhotoEffect {
    @Override
    public String getLabel () {
        return App.getContext().getString( R.string.photoeditor_effect_fisheye );
    }

    @Override
    public Effect createEffect ( EffectContext effectContext ) {
        Effect fishEye = effectContext.getFactory().createEffect( EffectFactory.EFFECT_FISHEYE );
        fishEye.setParameter( "scale", 1f );
        return fishEye;
    }
}

package papagaio.app.model.post;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;

public class ListHashTagPostRequest extends Request< Post.List > {

    private final String hashtag;

    public ListHashTagPostRequest ( String hashtag ) {
        super( Post.List.class );
        this.hashtag = hashtag;
    }

    @Override
    public Object getCacheKey () {
        return "hashtag." + hashtag + ".post";
    }

    @Override
    public Post.List loadDataFromNetwork () throws Exception {
        return RestAPI.getInstance().post.findAllByHashTag( hashtag ).execute().body();
    }
}

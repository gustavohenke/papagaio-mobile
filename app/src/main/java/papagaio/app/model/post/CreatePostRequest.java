package papagaio.app.model.post;

import android.content.ContentResolver;
import android.net.Uri;

import com.google.gson.Gson;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.util.Objects;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;
import retrofit.Call;
import retrofit.http.Multipart;

public class CreatePostRequest extends Request< Post > {

    private final Post post;
    private Uri photo;
    private ContentResolver contentResolver;

    public CreatePostRequest ( Post post ) {
        super( Post.class );
        this.post = post;
    }

    public void setPhoto ( ContentResolver resolver, Uri photo ) {
        Objects.requireNonNull( resolver );
        Objects.requireNonNull( photo );

        this.photo = photo;
        this.contentResolver = resolver;
    }

    @Override
    public long getDefaultCacheDuration () {
        return DurationInMillis.ALWAYS_EXPIRED;
    }

    @Override
    public Object getCacheKey () {
        return "post.create";
    }

    @Override
    public Post loadDataFromNetwork () throws Exception {
        PostAPI api = RestAPI.getInstance().post;
        Call< Post > call;

        if ( photo != null ) {
            final byte[] data = IOUtils.toByteArray( contentResolver.openInputStream( photo ) );

            String type = contentResolver.getType( photo );
            if ( type == null ) {
                // Tipo null deve ser, provavelmente, foto tirada pela câmera.
                // E fotos da câmera são sempre JPEG...
                type = "image/jpeg";
            }

            final MediaType mediaType = MediaType.parse( type );
            final RequestBody photoPart = RequestBody.create( mediaType, data );

            final RequestBody textPart = RequestBody.create( null, post.getText() );

            RequestBody locationPart = null;
            if ( post.getLocation() != null ) {
                String locationJson = new Gson().toJson( post.getLocation() );
                locationPart = RequestBody.create( null, locationJson );
            }

            call = api.create( textPart, locationPart, photoPart );
        } else {
            call = api.create( post );
        }

        return call.execute().body();
    }
}

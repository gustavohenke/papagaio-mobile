package papagaio.app.model.post;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;
import retrofit.Call;

public class ListUserPostRequest extends Request< Post.List > {

    private final int userId;
    private final String username;

    public ListUserPostRequest ( int userId ) {
        super( Post.List.class );
        this.userId = userId;
        this.username = null;
    }

    public ListUserPostRequest ( String username ) {
        super( Post.List.class );
        this.userId = 0;
        this.username = username;
    }

    @Override
    public Object getCacheKey () {
        return "user." + userId + ".post";
    }

    @Override
    public Post.List loadDataFromNetwork () throws Exception {
        PostAPI postAPI = RestAPI.getInstance().post;
        Call< Post.List > call = username != null ?
                postAPI.findAllByUser( username ) :
                postAPI.findAllByUser( userId );

        return call.execute().body();
    }
}

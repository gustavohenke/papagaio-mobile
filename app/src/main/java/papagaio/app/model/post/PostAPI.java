package papagaio.app.model.post;

import com.squareup.okhttp.RequestBody;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;

public interface PostAPI {

    @GET("post")
    Call< Post.List > findAll ();

    @GET("hashtag/{hashtag}/post")
    Call< Post.List > findAllByHashTag ( @Path( "hashtag" ) String hashtag );

    @GET("user/{userId}/post")
    Call< Post.List > findAllByUser ( @Path( "userId" ) int userId );

    @GET("user/{username}/post")
    Call< Post.List > findAllByUser ( @Path( "username" ) String username );

    @POST("post")
    Call< Post > create ( @Body Post post );

    @Multipart
    @POST("post")
    Call< Post > create ( @Part( "text" ) RequestBody text,
                          @Part( "location" ) RequestBody location,
                          @Part( "photo\"; filename=\"photo" ) RequestBody photo );

}

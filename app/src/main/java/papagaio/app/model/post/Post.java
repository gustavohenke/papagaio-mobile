package papagaio.app.model.post;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;

import papagaio.app.model.location.Location;
import papagaio.app.model.misc.Image;
import papagaio.app.model.user.User;

public class Post implements Parcelable {

    private long id;
    private User author;
    private String text;
    private Image.List photos;
    private Location location;
    private Date createdAt;
    private int likesCount;
    private boolean liked;
    private long replyTo;

    public Post () {
    }

    public User getAuthor () {
        return author;
    }

    public void setAuthor ( User author ) {
        this.author = author;
    }

    public Date getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt ( Date createdAt ) {
        this.createdAt = createdAt;
    }

    public long getId () {
        return id;
    }

    public void setId ( long id ) {
        this.id = id;
    }

    public Image.List getPhotos () {
        return photos;
    }

    public void setPhotos ( Image.List photos ) {
        this.photos = photos;
    }

    public Location getLocation () {
        return location;
    }

    public void setLocation ( Location location ) {
        this.location = location;
    }

    public long getReplyTo () {
        return replyTo;
    }

    public void setReplyTo ( long replyTo ) {
        this.replyTo = replyTo;
    }

    public String getText () {
        return text;
    }

    public void setText ( String text ) {
        this.text = text;
    }

    public int getLikesCount () {
        return likesCount;
    }

    public void setLikesCount ( int likesCount ) {
        this.likesCount = likesCount;
    }

    public boolean isLiked () {
        return liked;
    }

    public void setLiked ( boolean liked ) {
        this.liked = liked;
    }

    // ---------------------------------------------------------------------------------------------

    protected Post ( Parcel in ) {
        id = in.readLong();
        author = ( User ) in.readValue( User.class.getClassLoader() );
        text = in.readString();
        location = ( Location ) in.readValue( Location.class.getClassLoader() );
        in.readTypedList( photos, Image.CREATOR );
        long tmpCreatedAt = in.readLong();
        createdAt = tmpCreatedAt != -1 ? new Date( tmpCreatedAt ) : null;
        replyTo = in.readLong();
        likesCount = in.readInt();
        liked = in.readByte() != 0;
    }

    @Override
    public int describeContents () {
        return 0;
    }

    @Override
    public void writeToParcel ( Parcel dest, int flags ) {
        dest.writeLong( id );
        dest.writeValue( author );
        dest.writeString( text );
        dest.writeValue( location );
        dest.writeTypedList( photos );
        dest.writeLong( createdAt != null ? createdAt.getTime() : -1L );
        dest.writeLong( replyTo );
        dest.writeInt( likesCount );
        dest.writeByte( ( byte ) ( liked ? 1 : 0 ) );
    }

    public static final Creator< Post > CREATOR = new Creator< Post >() {
        @Override
        public Post createFromParcel ( Parcel in ) {
            return new Post( in );
        }

        @Override
        public Post[] newArray ( int size ) {
            return new Post[ size ];
        }
    };

    // ---------------------------------------------------------------------------------------------

    public static class List extends ArrayList< Post > {}

}

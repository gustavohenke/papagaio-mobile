package papagaio.app.model.post;

import papagaio.app.model.Request;
import papagaio.app.rest.RestAPI;
import retrofit.Response;

public class ListPostRequest extends Request< Post.List > {

    public ListPostRequest () {
        super( Post.List.class );
    }

    @Override
    public Post.List loadDataFromNetwork () throws Exception {
        PostAPI api = RestAPI.getInstance().post;
        Response< Post.List > call = api.findAll().execute();
        return call.body();
    }

    @Override
    public String getCacheKey () {
        return "listpost";
    }
}

package papagaio.app.util;

import android.os.Handler;
import android.os.Looper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * <p>Classe utilitária para executar {@code Runnable}s com debounce.</p>
 * <p>
 *     Executa um {@code Runnable} apenas depois que {@code interval} milissegundos tenham passado
 *     desde a última vez que {@code #call} foi chamado.
 * </p>
 * <p>
 *     É importante executar {@link Debouncer#terminate()} depois de pronto para que a instância
 *     adjacente de {@link ExecutorService} seja terminada.
 * </p>
 */
public class Debouncer {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool( 1 );
    private final Handler handler = new Handler( Looper.myLooper() );
    private final long interval;
    private ScheduledFuture future;

    /**
     * Instancia um novo Debouncer com o intervalo passado.
     * @param interval  A quantidade de tempo em milissegundos para aguardar até a execução de
     *                  um {@code Runnable}.
     */
    public Debouncer ( long interval ) {
        this.interval = interval;
    }

    /**
     * Executa {@code runnable} depois de {@code interval} milissegundos.
     *
     * <p>
     *     Se {@code #call} for chamado novamente neste meio tempo, a execução do {@code Runnable}
     *     anterior será cancelado.
     * </p>
     *
     * @param runnable  Um {@code Runnable} para agendar a execução neste Debouncer
     */
    public void call ( Runnable runnable ) {
        if ( future != null ) {
            future.cancel( false );
        }

        future = scheduler.schedule( new RunOnLooper( runnable ), interval, TimeUnit.MILLISECONDS );
    }

    /**
     * Finaliza esta instânica de Debouncer e da da instância adjacente de {@link ExecutorService}.
     */
    public void terminate () {
        scheduler.shutdown();
    }

    // ---------------------------------------------------------------------------------------------

    class RunOnLooper implements Runnable {

        private final Runnable runnable;

        public RunOnLooper ( Runnable runnable ) {
            this.runnable = runnable;
        }

        @Override
        public void run () {
            handler.post( runnable );
        }
    }
}

package papagaio.app.util.file;

import java.io.File;
import java.io.IOException;

public interface TempFileManager {

    /**
     * Gera o próximo arquivo temporário
     *
     * @return O arquivo temporário criado
     * @throws IOException  Caso ocorra algum erro durante a criação do arquivo temporário.
     */
    File next() throws IOException;

    /**
     *
     * @return  O último arquivo temporário gerado
     */
    File last();

    /**
     * Destroi todos os arquivos temporários gerados nesta instância.
     */
    void destroy();

}

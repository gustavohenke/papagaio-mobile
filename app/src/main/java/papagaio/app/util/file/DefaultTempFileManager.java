package papagaio.app.util.file;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import papagaio.app.util.Constants;

public class DefaultTempFileManager implements TempFileManager {

    private Queue< File > createdFiles = new LinkedList<>();
    private File last;

    @Override
    public File next () throws IOException {
        File file = File.createTempFile( System.currentTimeMillis() + "", "", Constants.FILES_ROOT );
        createdFiles.add( file );
        last = file;
        return file;
    }

    @Override
    public File last () {
        return last;
    }

    @Override
    public void destroy () {
        while ( !createdFiles.isEmpty() ) {
            File file = createdFiles.remove();

            //noinspection ResultOfMethodCallIgnored
            file.delete();
        }
    }
}

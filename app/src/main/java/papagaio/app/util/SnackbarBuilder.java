package papagaio.app.util;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import papagaio.app.R;

public class SnackbarBuilder {

    private final View parent;
    private String text;
    private int textResourceId;
    private int duration = Snackbar.LENGTH_INDEFINITE;

    public SnackbarBuilder ( View parent ) {
        this.parent = parent;
    }

    public SnackbarBuilder duration ( int duration ) {
        this.duration = duration;
        return this;
    }

    public SnackbarBuilder text ( String text ) {
        this.text = text;
        return this;
    }

    public SnackbarBuilder textResourceId ( int resId ) {
        this.textResourceId = resId;
        return this;
    }

    public Snackbar build ( Context context ) {
        Snackbar snackbar;

        if ( textResourceId != 0 ) {
            snackbar = Snackbar.make( parent, textResourceId, duration );
        } else {
            snackbar = Snackbar.make( parent, text, duration );
        }

        TextView text = ( TextView ) snackbar.getView().findViewById( android.support.design.R.id.snackbar_text );
        text.setTextAppearance( context, R.style.TextAppearance_App_Snackbar_Message );

        return snackbar;
    }
}

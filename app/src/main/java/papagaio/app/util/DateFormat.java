package papagaio.app.util;

import android.content.Context;

import java.util.Calendar;
import java.util.Date;

import papagaio.app.App;
import papagaio.app.R;

public class DateFormat {

    private final static Context context = App.getContext();

    private DateFormat () {
        throw new UnsupportedOperationException();
    }

    public static String format ( Date date ) {
        String format = getFormat( date );
        return String.valueOf( android.text.format.DateFormat.format( format, date ) );
    }

    private static String getFormat ( Date date ) {
        final long timestamp = date.getTime();

        Calendar endOfDay = Calendar.getInstance();
        endOfDay.set( Calendar.HOUR_OF_DAY, 23 );
        endOfDay.set( Calendar.MINUTE, 59 );
        endOfDay.set( Calendar.SECOND, 59 );
        endOfDay.set( Calendar.MILLISECOND, 999 );

        Calendar startOfDay = Calendar.getInstance();
        startOfDay.set( Calendar.HOUR_OF_DAY, 0 );
        startOfDay.set( Calendar.MINUTE, 0 );
        startOfDay.set( Calendar.SECOND, 0 );
        startOfDay.set( Calendar.MILLISECOND, 0 );

        boolean sameDay = startOfDay.getTimeInMillis() <= timestamp
                && endOfDay.getTimeInMillis() >= timestamp;

        if ( sameDay ) {
            return context.getString( R.string.dateformat_same_day );
        }

        Calendar startOfYear = ( Calendar ) startOfDay.clone();
        startOfYear.set( Calendar.MONTH, Calendar.JANUARY );
        startOfYear.set( Calendar.DAY_OF_MONTH, 1 );

        Calendar endOfYear = ( Calendar ) endOfDay.clone();
        endOfYear.set( Calendar.MONTH, endOfYear.getActualMaximum( Calendar.MONTH ) );
        endOfYear.set( Calendar.DAY_OF_MONTH, endOfYear.getActualMaximum( Calendar.DAY_OF_MONTH ) );

        boolean sameYear = startOfYear.getTimeInMillis() <= timestamp
                && endOfYear.getTimeInMillis() >= timestamp;

        if ( sameYear ) {
            return context.getString( R.string.dateformat_same_year );
        }

        return context.getString( R.string.dateformat_different_year );
    }

}

package papagaio.app.util.exceptionParser;

import com.octo.android.robospice.exception.NoNetworkException;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.LinkedHashMap;
import java.util.Map;

import papagaio.app.model.location.LocationNotAllowedException;
import papagaio.app.model.location.LocationNotAvailableException;
import papagaio.app.rest.exception.AuthenticationException;

public interface ExceptionParser {

    /**
     * Define a exceção para interpretar
     * @param exception
     */
    void setException( Exception exception );

    /**
     * @return  Retorna o título da exceção
     */
    String getTitle();

    /**
     * @return  Retorna a descrição da exceção
     */
    String getDescription();

    class Creator {
        private final static Map<
                Class< ? extends Exception >,
                Class< ? extends ExceptionParser >
            > mappings = new LinkedHashMap<>();

        static {
            mappings.put( ConnectException.class, ConnectExceptionParser.class );
            mappings.put( UnknownHostException.class, ConnectExceptionParser.class );
            mappings.put( SocketTimeoutException.class, SocketTimeoutExceptionParser.class );
            mappings.put( NoNetworkException.class, SocketTimeoutExceptionParser.class );
            mappings.put( AuthenticationException.class, AuthenticationExceptionParser.class );
            mappings.put( LocationNotAvailableException.class, LocationNotAvailableExceptionParser.class );
            mappings.put( LocationNotAllowedException.class, LocationNotAllowedExceptionParser.class );
            mappings.put( Exception.class, DefaultExceptionParser.class );
        }

        /**
         * Cria uma instância de {@code ExceptionParser} que sirva para o objeto {@code exception}
         * passado.
         *
         * @param   exception
         * @return
         */
        public static ExceptionParser create ( Exception exception ) {
            ExceptionParser parser = null;

            try {
                for ( Class<? extends Exception> exceptionCls : mappings.keySet() ) {
                    if ( exceptionCls.isInstance( exception ) ) {
                        parser = mappings.get( exceptionCls ).newInstance();
                        break;
                    }
                }

                if ( parser != null ) {
                    parser.setException( exception );
                }
            } catch ( Exception e ) {
                e.printStackTrace();
            }

            return parser;
        }
    }
}
package papagaio.app.util.exceptionParser;

import papagaio.app.App;
import papagaio.app.R;

class LocationNotAllowedExceptionParser implements ExceptionParser {
    @Override
    public void setException ( Exception exception ) {}

    @Override
    public String getTitle () {
        return App.getContext().getString( R.string.exception_locationnotallowed_title );
    }

    @Override
    public String getDescription () {
        return App.getContext().getString( R.string.exception_locationnotallowed_description );
    }
}

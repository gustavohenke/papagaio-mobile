package papagaio.app.util.exceptionParser;

import papagaio.app.App;
import papagaio.app.R;

class LocationNotAvailableExceptionParser implements ExceptionParser {
    @Override
    public void setException ( Exception exception ) {}

    @Override
    public String getTitle () {
        return App.getContext().getString( R.string.exception_locationnotavailable_title );
    }

    @Override
    public String getDescription () {
        return App.getContext().getString( R.string.exception_locationnotavailable_description );
    }
}

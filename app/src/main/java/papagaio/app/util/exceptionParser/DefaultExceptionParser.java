package papagaio.app.util.exceptionParser;

import papagaio.app.App;
import papagaio.app.R;

class DefaultExceptionParser implements ExceptionParser {

    @Override
    public void setException ( Exception exception ) {
        // Como este ExceptionParser lida apenas com Exceptions desconhecidas, printar o stacktrace
        // aqui me parece razoável...
        exception.printStackTrace();
    }

    @Override
    public String getTitle () {
        return App.getContext().getString( R.string.exception_default_title );
    }

    @Override
    public String getDescription () {
        return App.getContext().getString( R.string.exception_default_description );
    }
}

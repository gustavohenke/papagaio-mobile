package papagaio.app.util.exceptionParser;

import papagaio.app.App;
import papagaio.app.R;

class AuthenticationExceptionParser implements ExceptionParser {

    @Override
    public void setException ( Exception exception ) {}

    @Override
    public String getTitle () {
        return App.getContext().getString( R.string.exception_authentication_title );
    }

    @Override
    public String getDescription () {
        return App.getContext().getString( R.string.exception_authentication_desciption );
    }
}

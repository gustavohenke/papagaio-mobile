package papagaio.app.util.exceptionParser;

import papagaio.app.App;
import papagaio.app.R;
import papagaio.app.util.Utils;

class ConnectExceptionParser implements ExceptionParser {
    @Override
    public void setException ( Exception exception ) {}

    @Override
    public String getTitle () {
        return App.getContext().getString( R.string.exception_connect_title );
    }

    @Override
    public String getDescription () {
        int key = Utils.isNetworkAvailable() ? R.string.exception_connect_description_connected
                                             : R.string.exception_connect_description_disconnected;
        return App.getContext().getString( key );
    }
}

package papagaio.app.util;

import android.content.Intent;
import android.view.View;

public class LaunchIntentClickListener implements View.OnClickListener {
    private final Intent intent;

    public LaunchIntentClickListener ( Intent intent ) {
        this.intent = intent;
    }

    @Override
    public void onClick ( View view ) {
        view.getContext().startActivity( intent );
    }
}

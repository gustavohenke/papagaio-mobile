package papagaio.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import papagaio.app.App;

public final class Utils {

    public static boolean isNetworkAvailable () {
        Context context = App.getContext();
        ConnectivityManager manager = ( ConnectivityManager ) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }

    public static String getLoggedUsername () {
        SharedPreferences prefs = App.getContext().getSharedPreferences(
                Constants.PREFS_APP,
                Context.MODE_PRIVATE
        );

        return prefs.getString( Constants.PREFS_APP_USERNAME, "" );
    }

}

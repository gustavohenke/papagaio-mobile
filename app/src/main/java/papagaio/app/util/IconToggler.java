package papagaio.app.util;

import android.content.Context;
import android.content.res.Resources;
import android.widget.ImageView;

import papagaio.app.R;

public class IconToggler {

    private final Resources resources;

    public IconToggler ( Context context ) {
        this.resources = context.getResources();
    }

    public void toggle ( boolean toggle, ImageView icon ) {
        if ( toggle ) {
            activate( icon );
        } else {
            deactivate( icon );
        }
    }

    public void activate ( ImageView icon ) {
        icon.setAlpha( resources.getFraction( R.fraction.toggler_icon_alpha_active, 1, 1 ) );
        icon.setColorFilter( resources.getColor( R.color.toggler_icon_tint_active ) );
    }

    public void deactivate ( ImageView icon ) {
        icon.setAlpha( resources.getFraction( R.fraction.toggler_icon_alpha, 1, 1 ) );
        icon.setColorFilter( null );
    }

}

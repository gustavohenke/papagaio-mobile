package papagaio.app.util.exceptionHandler;

import android.content.Context;

public interface ExceptionHandler {

    void handle ( Context context, Exception e );

}

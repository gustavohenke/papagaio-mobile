package papagaio.app.util.exceptionHandler;

import android.app.AlertDialog;
import android.content.Context;

import papagaio.app.R;
import papagaio.app.util.exceptionParser.ExceptionParser;

public class DialogExceptionHandler implements ExceptionHandler {
    @Override
    public void handle ( Context context, Exception e ) {
        ExceptionParser parser = ExceptionParser.Creator.create( e );
        new AlertDialog.Builder( context )
                .setTitle( parser.getTitle() )
                .setMessage( parser.getDescription() )
                .show();
    }
}

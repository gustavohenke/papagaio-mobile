package papagaio.app.util.exceptionHandler;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

import papagaio.app.util.SnackbarBuilder;
import papagaio.app.util.exceptionParser.ExceptionParser;

public class SnackbarExceptionHandler implements ExceptionHandler {

    private final int duration;
    private final View parent;

    public SnackbarExceptionHandler ( View parent ) {
        this( parent, Snackbar.LENGTH_INDEFINITE );
    }

    public SnackbarExceptionHandler ( View parent, int duration ) {
        this.duration = duration;
        this.parent = parent;
    }

    @Override
    public void handle ( Context context, Exception e ) {
        ExceptionParser parser = ExceptionParser.Creator.create( e );
        new SnackbarBuilder( parent )
                .text( parser.getTitle() )
                .duration( duration )
                .build( context )
                .show();
    }
}

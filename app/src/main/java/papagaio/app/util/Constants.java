package papagaio.app.util;

import android.os.Environment;

import java.io.File;

import papagaio.app.App;
import papagaio.app.R;

public interface Constants extends BaseConstants {

    String API_URL = URL_HOST + App.getContext().getString( R.string.path_api );
    String URL_HASHTAG = URL_HOST + App.getContext().getString( R.string.path_prefix_hashtag );
    String URL_APP_HASHTAG = App.getContext().getString( R.string.scheme_app ) + "://" +
                            App.getContext().getString( R.string.host_timeline ) +
                            App.getContext().getString( R.string.path_prefix_hashtag );
    String URL_APP_USERNAME = App.getContext().getString( R.string.scheme_app ) + "://" +
                            App.getContext().getString( R.string.host_user ) + "/";

    String ACCOUNT_TYPE = App.getContext().getPackageName();
    String TOKEN_TYPE = "normal";

    String PREFS_APP = "APP";
    String PREFS_APP_USERNAME = "username";
    String PREFS_APP_TOKEN = "token";

    int MAX_CHARS = 150;

    File FILES_ROOT = new File( Environment.getExternalStorageDirectory(), "papagaio" );
}

package papagaio.app.util.intent;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import papagaio.app.R;
import papagaio.app.util.file.TempFileManager;

public class SelectPhotoIntentFactory implements IntentFactory {

    private final TempFileManager tempFileManager;

    public SelectPhotoIntentFactory ( TempFileManager tempFileManager ) {
        this.tempFileManager = tempFileManager;
    }

    public Intent create ( Context context ) {
        try {
            final File file = tempFileManager.next();
            final Uri uri = Uri.fromFile( file );

            Intent pickIntent = new Intent();
            pickIntent.setAction( Intent.ACTION_GET_CONTENT );
            pickIntent.setType( "image/*" );

            // TODO http://stackoverflow.com/questions/4455558/allow-user-to-select-camera-or-gallery-for-image/12347567#12347567
            final List< Intent > cameraIntents = new ArrayList<>();
            final Intent takePhotoIntent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
            final PackageManager pkgManager = context.getPackageManager();
            final List< ResolveInfo > resolvedCameras = pkgManager.queryIntentActivities( takePhotoIntent, 0 );

            for ( ResolveInfo resolvedCamera : resolvedCameras ) {
                String pkgName = resolvedCamera.activityInfo.packageName;
                Intent intent = new Intent( takePhotoIntent );
                intent.setComponent( new ComponentName( pkgName, resolvedCamera.activityInfo.name ) );
                intent.setPackage( pkgName );
                intent.putExtra( MediaStore.EXTRA_OUTPUT, uri );
                cameraIntents.add( intent );
            }

            String title = context.getString( R.string.selectphoto_title );
            Intent chooserIntent = Intent.createChooser( pickIntent, title );
            chooserIntent.putExtra(
                    Intent.EXTRA_INITIAL_INTENTS,
                    cameraIntents.toArray( new Intent[ cameraIntents.size() ] )
            );

            return chooserIntent;
        } catch ( IOException e ) {
            return null;
        }
    }

}

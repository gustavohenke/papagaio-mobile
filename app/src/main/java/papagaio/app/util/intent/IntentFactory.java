package papagaio.app.util.intent;

import android.content.Context;
import android.content.Intent;

public interface IntentFactory {

    Intent create( Context context );
}

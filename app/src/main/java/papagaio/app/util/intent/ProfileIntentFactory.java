package papagaio.app.util.intent;

import android.content.Context;
import android.content.Intent;

import papagaio.app.feature.profile.ProfileActivity;

public class ProfileIntentFactory implements IntentFactory {
    private final int id;

    public ProfileIntentFactory ( int id ) {
        this.id = id;
    }

    @Override
    public Intent create ( Context context ) {
        return ProfileActivity.newIntent( context, id );
    }
}

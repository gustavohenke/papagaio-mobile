package papagaio.app.authenticator;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import papagaio.app.feature.login.LoginActivity;
import papagaio.app.model.session.Credentials;
import papagaio.app.model.session.LoginRequest;
import papagaio.app.service.AuthService;

public class AccountAuthenticator extends AbstractAccountAuthenticator {

    private static final Class< ? extends Activity > loginActivity = LoginActivity.class;
    private final Context context;

    public AccountAuthenticator ( Context context ) {
        super( context );
        this.context = context;
    }

    @Override
    public Bundle editProperties ( AccountAuthenticatorResponse response, String accountType ) {
        return null;
    }

    @Override
    public Bundle addAccount ( AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options ) throws NetworkErrorException {
        final Intent intent = new Intent( this.context, loginActivity );
        intent.putExtra( AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response );
//        intent.putExtra(AuthenticatorActivity.ARG_ACCOUNT_TYPE, accountType);
//        intent.putExtra(AuthenticatorActivity.ARG_AUTH_TYPE, authTokenType);
//        intent.putExtra(AuthenticatorActivity.ARG_IS_ADDING_NEW_ACCOUNT, true);

        final Bundle bundle = new Bundle();
        bundle.putParcelable( AccountManager.KEY_INTENT, intent );
        return bundle;
    }

    @Override
    public Bundle confirmCredentials ( AccountAuthenticatorResponse response, Account account, Bundle options ) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken ( AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options ) throws NetworkErrorException {
        AccountManager accountManager = AccountManager.get( this.context );
        String authToken = accountManager.peekAuthToken( account, authTokenType );

        // Não temos um authToken, mas podemos fazer mais uma tentativa de autenticação, com usuário
        // e senha salvos na conta
        if ( TextUtils.isEmpty( authToken ) ) {
            String password = accountManager.getPassword( account );
            if ( password != null ) {
                Credentials credentials = new Credentials( account.name, password );

                try {
                    authToken = new LoginRequest( credentials ).executeSync();
                } catch ( Exception ignored ) {}
            }
        }

        if ( !TextUtils.isEmpty( authToken ) ) {
            Bundle result = new Bundle();
            result.putString( AccountManager.KEY_ACCOUNT_NAME, account.name );
            result.putString( AccountManager.KEY_ACCOUNT_TYPE, account.type );
            result.putString( AccountManager.KEY_AUTHTOKEN, authToken );

            // Define este token, tido como válido, para uso na autenticação da API.
            AuthService.setAuth( authToken );
        }

        Intent intent = new Intent( this.context, loginActivity );
        intent.putExtra( AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response );
//        intent.putExtra(AuthenticatorActivity.ARG_ACCOUNT_TYPE, account.type);
//        intent.putExtra(AuthenticatorActivity.ARG_AUTH_TYPE, authTokenType);

        Bundle result = new Bundle();
        result.putParcelable( AccountManager.KEY_INTENT, intent );
        return result;
    }

    @Override
    public String getAuthTokenLabel ( String authTokenType ) {
        return null;
    }

    @Override
    public Bundle updateCredentials ( AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options ) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle hasFeatures ( AccountAuthenticatorResponse response, Account account, String[] features ) throws NetworkErrorException {
        Bundle bundle = new Bundle();
        bundle.putBoolean( AccountManager.KEY_BOOLEAN_RESULT, false );
        return bundle;
    }
}

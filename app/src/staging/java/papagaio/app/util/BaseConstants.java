package papagaio.app.util;

import papagaio.app.App;
import papagaio.app.R;

public interface BaseConstants {

    String URL_HOST = App.getContext().getString( R.string.scheme ) + "://" +
                        App.getContext().getString( R.string.host ) + ":" +
                        App.getContext().getString( R.string.port );

}

# Papagaio Mobile

Papagaio Mobile - versão Android

## Requisitos
- Android SDK v23
- Android Studio 1.4

**Recomendado:** uso da [VM GenyMotion](https://www.genymotion.com/) para desenvolvimento.